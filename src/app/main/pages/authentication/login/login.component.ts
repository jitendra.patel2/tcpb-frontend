import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute,NavigationEnd } from "@angular/router";
import { MatSnackBar } from '@angular/material/snack-bar';
import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
import {AuthService} from '../../../../auth/auth.service';
@Component({
    selector     : 'login',
    templateUrl  : './login.component.html',
    styleUrls    : ['./login.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class LoginComponent implements OnInit
{
    loginForm: FormGroup;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private router: Router, private authService: AuthService,
        private route: ActivatedRoute,
        // private _snackBar: MatSnackBar


    )
    {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar   : {
                    hidden: true
                },
                toolbar  : {
                    hidden: true
                },
                footer   : {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
    }
    successMsg = '';

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */

    error : any;
    loginError : any;
    isLogin = false;

    ngOnInit(): void
    {
        this.successMsg = localStorage.getItem('sucessMsg');
    
    if(this.successMsg){
      this.openSnackBar(this.successMsg,'Success');
      localStorage.removeItem('sucessMsg');

    }
        this.loginForm = this._formBuilder.group({
            email   : ['', [Validators.required, Validators.email]],
            password: ['', Validators.required]
        });

        this.authService.logout();
        this.isLogin = false;
    }

    onSubmit() {
  
  
        const formData = new FormData();
        formData.append('email', this.loginForm.get('email').value);
        formData.append('password', this.loginForm.get('password').value);

        this.authService.login(formData).subscribe((data) => {
          if(data.status=="fail"){
  
              this.loginError = data.message;
  
          }else{
  
            if (data.data) {
              localStorage.setItem('currentUser', JSON.stringify(data.data));
              const redirect = this.authService.redirectUrl ? this.authService.redirectUrl : '/apps/dashboards/analytics';
              this.router.navigate([redirect]);
            }
          
          }
        },
        error => this.error = error
      );
    }
    openSnackBar(message: string, action: string) {
        // this._snackBar.open(message, action, {
        //   duration: 2000,
        // });
      }
}
