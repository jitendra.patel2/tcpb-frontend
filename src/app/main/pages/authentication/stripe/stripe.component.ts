import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '../../../../auth/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';
// import Swal from 'sweetalert2'
import Swal from 'sweetalert2/dist/sweetalert2.js';
@Component({
    selector: 'stripe',
    templateUrl: './stripe.component.html',
    styleUrls: ['./stripe.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations: fuseAnimations
})
export class StripeComponent implements OnInit {
    stripeForm: FormGroup;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private AuthService: AuthService,
        private router: Router,
        private route: ActivatedRoute
    ) {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar: {
                    hidden: true
                },
                toolbar: {
                    hidden: true
                },
                footer: {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
    }
    apiError: any;
    error: any;
    id:number;
    processorName:string;
    reportType:string;
    monthData = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {
        this.stripeForm = this._formBuilder.group({
            buTons: ['',  [Validators.required,Validators.pattern(/^[0-9]\d*(\.\d+)?$/)]],
            amount: ['', [Validators.required,Validators.pattern(/^[0-9]\d*(\.\d+)?$/)]],
            name: ['', [Validators.required]],
            title: ['', [Validators.required]],
            account_holder_name: ['', [Validators.required]],
            account_holder_type: ['', [Validators.required]],
            routing_number: ['', [Validators.required,Validators.pattern(/^[0-9-+()]*$/), Validators.minLength(9), Validators.maxLength(9)]],
            account_number: ['', [Validators.required,Validators.pattern(/^[0-9-+()]*$/)]],
            // number: ['', [Validators.required,Validators.pattern(/^[0-9-+()]*$/), Validators.minLength(16), Validators.maxLength(16)]],
            // exp_month: ['', [Validators.required,Validators.pattern(/^[0-9-+()]*$/), Validators.minLength(1), Validators.maxLength(2)]],
            // exp_year: ['', [Validators.required,Validators.pattern(/^[0-9-+()]*$/), Validators.minLength(4), Validators.maxLength(4)]],
            // cvc: ['', [Validators.required, Validators.maxLength(5), Validators.pattern("^[0-9]*$")]],
            // card_holder: ['', [Validators.required]],
            is_check: [false],
            startMonth: [''],
        });
        this.route.params.subscribe(params => {
            this.id = + params['id']; 
             this.processorName = params['name'];
             this.reportType = params['type'];
         });
         this.reportCheck();
        
    
    }

    public errorHandling = (control: string, error: string) => {
        return this.stripeForm.controls[control].hasError(error);
    }

    // On submit click, reset form fields
    onSubmit() {
        const formData = new FormData();
        formData.append('buTons', this.stripeForm.get('buTons').value);
        formData.append('amount', this.stripeForm.get('amount').value);
        formData.append('name', this.stripeForm.get('name').value);
        formData.append('title', this.stripeForm.get('title').value);
        formData.append('account_holder_name', this.stripeForm.get('account_holder_name').value);
        formData.append('account_holder_type', this.stripeForm.get('account_holder_type').value);
        formData.append('routing_number', this.stripeForm.get('routing_number').value);
        formData.append('account_number', this.stripeForm.get('account_number').value);
        formData.append('is_check', this.stripeForm.get('is_check').value);
        formData.append('startMonth', this.stripeForm.get('startMonth').value);

        this.AuthService.stripePayment(formData).subscribe(
            res => {
              if (res.status === 'fail') {
                // this.apiError = res.message;
                let message = res.message;
                let link = '';
                if(res && res.data && res.data.raw && res.data.raw.message){
                    message = res.data.raw.message;
                    link = '<a href =' + res.data.raw.doc_url +' >' +  res.data.raw.doc_url + '</a>'
                }
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong! ' + message,
                    footer: link
                  })
                // this.ngOnInit() ;
              } else {
                Swal.fire(
                    res.message,
                    '',
                    // 'You clicked the button!',
                    'success'
                  )
                  this.ngOnInit() ;
                localStorage.setItem('sucessMsg',res.message);
               // this.router.navigate(['/admin/login']);
              }
            },
            error => this.error = error
          );

    }


    reportCheck() {
        this.stripeForm.get('is_check').valueChanges
            .subscribe(value => {
                if (value) {
                    this.stripeForm.get('startMonth').setValidators([Validators.required]);
                    this.stripeForm.get('startMonth').updateValueAndValidity();
                } else {
                    this.stripeForm.get('startMonth').clearValidators();
                    this.stripeForm.get('startMonth').updateValueAndValidity();

                }
            }
            );
    }

    butonsFormula(value: Number){
        if(this.reportType == 'corn'){
            this.stripeForm.patchValue({
                amount:  (Number(value)*0.01).toFixed(0)  
         
              });
        } else {
            this.stripeForm.patchValue({
                amount:  (Number(value)*0.074).toFixed(0) 
              });
        }
       
    }
}
