import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { FuseSharedModule } from '@fuse/shared.module';
import {MatSelectModule} from '@angular/material/select';
import { StripeComponent } from 'app/main/pages/authentication/stripe/stripe.component';
import {MatCheckboxModule} from '@angular/material/checkbox'

const routes = [
    {
        path     : 'payment/:id/:name/:type',
        component: StripeComponent
    }
];

@NgModule({
    declarations: [
        StripeComponent
    ],
    imports     : [
        RouterModule.forChild(routes),

        MatButtonModule,
        MatFormFieldModule,
        MatIconModule,
        MatInputModule,
        MatSelectModule,
        MatCheckboxModule,

        FuseSharedModule
    ]
})
export class StripeModule
{
}
