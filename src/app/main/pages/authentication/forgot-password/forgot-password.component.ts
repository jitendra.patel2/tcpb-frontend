import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {AuthService} from '../../../../auth/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FuseConfigService } from '@fuse/services/config.service';
import { fuseAnimations } from '@fuse/animations';

@Component({
    selector     : 'forgot-password',
    templateUrl  : './forgot-password.component.html',
    styleUrls    : ['./forgot-password.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ForgotPasswordComponent implements OnInit
{
    forgotPasswordForm: FormGroup;

    /**
     * Constructor
     *
     * @param {FuseConfigService} _fuseConfigService
     * @param {FormBuilder} _formBuilder
     */
    constructor(
        private _fuseConfigService: FuseConfigService,
        private _formBuilder: FormBuilder,
        private AuthService:AuthService,
        private router: Router,
        private route: ActivatedRoute
    )
    {
        // Configure the layout
        this._fuseConfigService.config = {
            layout: {
                navbar   : {
                    hidden: true
                },
                toolbar  : {
                    hidden: true
                },
                footer   : {
                    hidden: true
                },
                sidepanel: {
                    hidden: true
                }
            }
        };
    }
    apiError : any;
    successMsg : any;
    error : any;
    buttonStatus = false;

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {
        this.forgotPasswordForm = this._formBuilder.group({
            email: ['', [Validators.required, Validators.email]]
        });
    }

    get email() { return this.forgotPasswordForm.get('email'); }

    // On submit click, reset form fields
    onSubmit() {

        this.buttonStatus = true;
        const formData = new FormData();
        formData.append('email', this.forgotPasswordForm.get('email').value);

        this.AuthService.forgetPassword(formData).subscribe(
            res => {
              if (res.status === 'fail') {
                this.apiError = res.message;
                this.ngOnInit() ;
              } else {
                this.successMsg = res.message;
                // localStorage.setItem('sucessMsg',res.message);
                // this.router.navigate(['/admin/login']);
              }
              this.buttonStatus = false;
            },
            error => this.error = error
          );
      
    }


    checkEmail(value: String){
        this.apiError = false;
        this.successMsg = false;
    }
    
}
