import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { DataSource } from '@angular/cdk/collections';
import { BehaviorSubject, Observable } from 'rxjs';
import * as shape from 'd3-shape';

import { fuseAnimations } from '@fuse/animations';
import {ProcessorsService} from '../../../../service/processors.service';

@Component({
    selector     : 'project-dashboard',
    templateUrl  : './project.component.html',
    styleUrls    : ['./project.component.scss'],
    encapsulation: ViewEncapsulation.None,
    animations   : fuseAnimations
})
export class ProjectDashboardComponent implements OnInit
{
    projects: any[];
    selectedProject: any;


    processor = '0';
    producer = '0';
    refund = '0';
    assesments = '0';
    reports = '0';
    error = '';
    constructor(
        private ProcessorsService : ProcessorsService
    )
    {
    

    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void
    {

        this.ProcessorsService.getDashboardCount().subscribe((data) => {
            this.processor  = data['processorCount'];
            this.producer  = data['producerCount'];
            this.refund  = data['refundCount'];
            this.assesments  = data['assesmentsCount'];
            this.reports  = data['reportsCount'];

          },
            error => {this.error = error}
          );
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Toggle the sidebar
     *
     * @param name
     */

}


