import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { FuseSharedModule } from '@fuse/shared.module';
import {AuthGuard} from '../../auth/auth.guard';
const routes = [
    {
        path        : 'dashboards/analytics',
        canActivate: [AuthGuard],
        loadChildren: () => import('./dashboards/project/project.module').then(m => m.ProjectDashboardModule)
    },

    {
        path        : 'admin',
        canActivate: [AuthGuard],
        loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule)
    }
];

@NgModule({
    imports     : [
        RouterModule.forChild(routes),
        FuseSharedModule
    ]
})
export class AppsModule
{
}
