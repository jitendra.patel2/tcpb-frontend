import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation,Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import {MatSnackBar} from '@angular/material/snack-bar';
import { Subject } from 'rxjs';
import {ProcessorsService} from '../../../../service/processors.service';
import {UsersService} from '../../../../service/users.service';

import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-savequery-model',
  templateUrl: './savequery-model.component.html',
  styleUrls: ['./savequery-model.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class SavequeryModelComponent implements OnInit {

  private _unsubscribeAll: Subject<any>;
  /**
   * Constructor
   *
   * @param {FormBuilder} _formBuilder
   */
  constructor(
    private _formBuilder: FormBuilder,
    private ProcessorsService : ProcessorsService,
    private UsersService : UsersService,
    private router: Router,
    private route: ActivatedRoute,
    private _snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<SavequeryModelComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any)
{
    // Set the private defaults
    this._unsubscribeAll = new Subject();
}

form: FormGroup;
error = '';
formError = "";
  ngOnInit(): void {
     // Reactive Form
     this.form = this._formBuilder.group({
     topic      : ['', Validators.required],
  });
  }

  onNoClick(): void {
    this.dialogRef.close();
  
  }
   public errorHandling = (control: string, error: string) => {
     return this.form.controls[control].hasError(error);
   }
   onSubmit () {
  
  
     const formData = new FormData();
     formData.append('topic', this.form.get('topic').value);
     formData.append('status', this.data.status);
     formData.append('check_DateData', this.data.check_DateData);
     formData.append('check_DateData1', this.data.check_DateData1);
     formData.append('type', '0');
  
       this.ProcessorsService.saveQuery(formData).subscribe(
         res => {
           if (res.status === 'fail') {
             this.formError = res.message;
             this.openSnackBar(this.formError,'Error');
           } else {
            // localStorage.setItem('sucessMsg',res.message);
             this.openSnackBar(res.message,'Success');
            this.onNoClick();
           }
         },
         error => this.error = error
       );
     
   }
  
   openSnackBar(message: string, action: string) {
     this._snackBar.open(message, action, {
       duration: 2000,
     });
   }

}
