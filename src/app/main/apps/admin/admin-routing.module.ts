import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ManageProcessorsComponent } from './manage-processors/manage-processors.component';
import { ManageProducersComponent } from './manage-producers/manage-producers.component';
import { ManageUsersComponent } from './manage-users/manage-users.component';
import { ManageProfileComponent } from './manage-profile/manage-profile.component';
import { UsersFormComponent } from './users-form/users-form.component';
import { AdminFormComponent } from './admin-form/admin-form.component';
import { EmailFormComponent } from './email-form/email-form.component';
import { ProcessorsFormComponent } from './processors-form/processors-form.component';
import { ProducersFormComponent } from './producers-form/producers-form.component';
import { ManageRefundComponent } from './manage-refund/manage-refund.component';
import { ManageAissmentComponent } from './manage-aissment/manage-aissment.component';

import { AssessmentFormComponent } from './assessment-form/assessment-form.component';
import { RefundFormComponent } from './refund-form/refund-form.component';
import { ChangePasswordFormComponent } from './changePassword-form/changePassword-form.component';
import { ManageNotesComponent } from './manage-notes/manage-notes.component';
import { NotesFormComponent } from './notes-form/notes-form.component';
import { ManageViewAssessmentsComponent } from './manage-view-assessments/manage-view-assessments.component';
import { ManageViewRefundsComponent } from './manage-view-refunds/manage-view-refunds.component'; 
import { ManageReportComponent } from './manage-report/manage-report.component';
import { ManageSavequeryComponent } from './manage-savequery/manage-savequery.component';
import { ManageProcessorReportComponent } from './manage-processor-report/manage-processor-report.component';
import { ManageReportSendProcessorComponent } from './manage-report-send-processor/manage-report-send-processor.component';


const routes: Routes = [

  {
    path     : 'manage-processor-report/:id/:type',
    component: ManageProcessorReportComponent
  },

  {
    path     : 'manage-query',
    component: ManageSavequeryComponent
  },
  {
    path     : 'manage-reports',
    component: ManageReportComponent
  },
  {
    path     : 'manage-reports-procesor',
    component: ManageReportSendProcessorComponent
  },
  {
    path     : 'manage-view-assessments/:id/:type',
    component: ManageViewAssessmentsComponent
  },
  {
    path     : 'manage-view-refunds/:id/:type',
    component: ManageViewRefundsComponent
  },
  {
    path     : 'Edit-Notes/:id/:nId/:type',
    component: NotesFormComponent
  },
  {
    path     : 'Create-Notes/:id/:nId/:type',
    component: NotesFormComponent
  },
  {
    path     : 'Manage-Notes/:id/:type',
    component: ManageNotesComponent
  },
  {
    path     : 'Edit-Refund/:id',
    component: RefundFormComponent
  },
  {
    path     : 'Create-Refund',
    component: RefundFormComponent
  },
  {
    path     : 'Change-Password',
    component: ChangePasswordFormComponent
  },

  {
    path     : 'Edit-Assessment/:id',
    component: AssessmentFormComponent
  },
  {
    path     : 'Create-Assessment',
    component: AssessmentFormComponent
  },

  {
    path     : 'edit-Processors/:id',
    component: ProcessorsFormComponent
  },
  {
    path     : 'Create-Processors',
    component: ProcessorsFormComponent
  },
  {
    path     : 'edit-Producers/:id',
    component: ProducersFormComponent
  },
  {
    path     : 'Create-Producers',
    component: ProducersFormComponent
  },
  {
    path     : 'edit-User/:id',
    component: UsersFormComponent
  },
  {
    path     : 'Create-User',
    component: UsersFormComponent
  },
  {
    path     : 'edit-profile',
    component: AdminFormComponent
  },
  {
    path     : 'email',
    component: EmailFormComponent
  },
  {
    path     : 'manage-refund',
    component: ManageRefundComponent
  },{
    path     : 'manage-assessment',
    component: ManageAissmentComponent
  },{
    path     : 'manage-processors',
    component: ManageProcessorsComponent
  },
  {
    path     : 'manage-producers',
    component: ManageProducersComponent
  },
  {
    path     : 'manage-users',
    component: ManageUsersComponent
  },
  {
    path     : 'profile-detail',
    component: ManageProfileComponent
  }
  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
