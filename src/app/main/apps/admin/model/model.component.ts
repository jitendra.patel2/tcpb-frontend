import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation,Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import {MatSnackBar} from '@angular/material/snack-bar';
import { Subject } from 'rxjs';
import {ProcessorsService} from '../../../../service/processors.service';
import {UsersService} from '../../../../service/users.service';

import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';


@Component({
  selector: 'app-model',
  templateUrl: './model.component.html',
  styleUrls: ['./model.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})


export class ModelComponent implements OnInit {

  private _unsubscribeAll: Subject<any>;
  /**
   * Constructor
   *
   * @param {FormBuilder} _formBuilder
   */
  constructor(
    private _formBuilder: FormBuilder,
    private ProcessorsService : ProcessorsService,
    private UsersService : UsersService,
    private router: Router,
    private route: ActivatedRoute,
    private _snackBar: MatSnackBar,
    public dialogRef: MatDialogRef<ModelComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any)
{
    // Set the private defaults
    this._unsubscribeAll = new Subject();
}

form: FormGroup;

 accessTypeData = [
   {
     'id':1,
     'title' : 'Grain'
   },
   {
     'id':2,
     'title' : 'Silage'
   }
 ];

error = '';
formError = "";
buttonDisable = false
checkMonth = 0;
checkMonth1 = 0;
check_DateData = '0';
check_DateData1 = '0';
 ngOnInit(): void {

  console.log(this.data);
    // Reactive Form
    this.form = this._formBuilder.group({
     email  : ['', [Validators.required, Validators.maxLength(250),
       Validators.minLength(5),
Validators.pattern(/.+@.+\..+/)]],
    subject      : ['', Validators.required],
    body      : ['', Validators.required]
 });
 this.form.patchValue({
  email : (this.data.email) ? this.data.email : ''
 });


 }
 onNoClick(): void {
  this.dialogRef.close();

}
 public errorHandling = (control: string, error: string) => {
   return this.form.controls[control].hasError(error);
 }
 onSubmit () {
  this.buttonDisable = true;

   const formData = new FormData();
   formData.append('email', this.form.get('email').value);
   formData.append('subject', this.form.get('subject').value);
   formData.append('body', this.form.get('body').value);
   formData.append('accessType',this.data.accessType);
   formData.append('check_Date', this.data.check_DateData);
   formData.append('check_Date1', this.data.check_DateData1);
   formData.append('processorId', this.data.processorId);
   formData.append('data', JSON.stringify(this.data.exportData));
   formData.append('type', this.data.type);

     this.ProcessorsService.sendAssmentReport(formData).subscribe(
       res => {
        this.openSnackBar(res.message,'Success');
         if (res.status === 'fail') {
          this.buttonDisable = false;
           this.formError = res.message;
           this.openSnackBar(this.formError,'Error');
         } else {
          this.buttonDisable = false;
          // localStorage.setItem('sucessMsg',res.message);
          this.onNoClick();
         }
       },
       error => this.error = error
     );
   
 }

 openSnackBar(message: string, action: string) {
   this._snackBar.open(message, action, {
     duration: 2000,
   });
 }


}
