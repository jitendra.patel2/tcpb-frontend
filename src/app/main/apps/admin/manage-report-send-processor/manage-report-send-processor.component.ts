import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {RefundService} from '../../../../service/refund.service'
import {UsersService} from '../../../../service/users.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';

import { ExportToCsv } from 'export-to-csv';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {SavequeryModelComponent} from '../savequery-model/savequery-model.component';

@Component({
  selector: 'app-manage-report-send-processor',
  templateUrl: './manage-report-send-processor.component.html',
  styleUrls: ['./manage-report-send-processor.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class ManageReportSendProcessorComponent implements OnInit {

 // Private
  // Private
/**
 * Constructor
 *
 * @param {FormBuilder} _formBuilder
 */
displayedColumns: string[] = ['id','processor', 'assmentcount'];
dataSource = new MatTableDataSource();
@ViewChild(MatPaginator) paginator: MatPaginator;


constructor(
  private _formBuilder: FormBuilder,
  private route: ActivatedRoute,
  private RefundService : RefundService,
  private UsersService : UsersService,
  private _snackBar: MatSnackBar,
  private router: Router,
  public dialog: MatDialog
  ) { }
  title = 'Report Management';
  successMsg = '';
  error = {};
  form: FormGroup;
  processorData : [];
producerData : [];
checkMonth = 0;
checkMonth1 = 0;
check_DateData = '0';
check_DateData1 = '0';
processorCount = '0';
isLoading = false;
STEmail = false;
exportData = [];
sType = "Assessments";


pTypeData = [
  {
    'id':1,
    'title' : 'Elevator'
  },
  {
    'id':2,
    'title' : 'FSA Office'
  }
];

  ngOnInit(): void {
    
    this.successMsg = localStorage.getItem('sucessMsg');

    if(this.successMsg){
      this.openSnackBar(this.successMsg,'Success');
      localStorage.removeItem('sucessMsg');

    }
    localStorage.removeItem('processor_id');
    localStorage.removeItem('producer_Id');

    // Reactive Form
    this.form = this._formBuilder.group({
      processor_id : ['',Validators.required],
      STEmail  : ['',Validators.required],
      processor_type  : ['',Validators.required],
      type : ['1'],
      check_Date  : [''],
      check_Date1 : ['']
    });
   

  }

  saveQuery(){
    if(((this.form.get('check_Date1').value)._i)){
      
      this.checkMonth1 = (((this.form.get('check_Date1').value)._i).month.length!=2) ? '0'+((this.form.get('check_Date1').value)._i).month : ((this.form.get('check_Date1').value)._i).month;

      this.checkMonth1 = (this.checkMonth1<13) ? Number(this.checkMonth1)+Number(1) :  this.checkMonth1;

      this.checkMonth1 = (((this.checkMonth1).toString()).length!=2) ? 0+this.checkMonth1 : this.checkMonth1;

      this.check_DateData1 = ((this.form.get('check_Date1').value)._i).year+'-'+this.checkMonth1+'-'+((this.form.get('check_Date1').value)._i).date;

    }else{
      this.check_DateData1 = this.form.get('check_Date1').value
    }

    this.check_DateData1 = (this.check_DateData1) ? this.check_DateData1 : '0';

    if(((this.form.get('check_Date').value)._i)){
      
      this.checkMonth = (((this.form.get('check_Date').value)._i).month.length!=2) ? '0'+((this.form.get('check_Date').value)._i).month : ((this.form.get('check_Date').value)._i).month;

      this.checkMonth = (this.checkMonth<13) ? Number(this.checkMonth)+Number(1) :  this.checkMonth;

      this.checkMonth = (((this.checkMonth).toString()).length!=2) ? 0+this.checkMonth : this.checkMonth;

      this.check_DateData = ((this.form.get('check_Date').value)._i).year+'-'+this.checkMonth+'-'+((this.form.get('check_Date').value)._i).date;

    }else{
      this.check_DateData = this.form.get('check_Date').value;
    }

    

    this.check_DateData = (this.check_DateData) ? this.check_DateData : '0';

   const status = '0';
   
    const dialogRef = this.dialog.open(SavequeryModelComponent, {
      width: '100%',
      data : {status:status,check_DateData : this.check_DateData,check_DateData1 : this.check_DateData1}
    });
  }

  filterChange(){
   
    // if(((this.form.get('check_Date1').value)._i)){
      
    //   this.checkMonth1 = (((this.form.get('check_Date1').value)._i).month.length!=2) ? '0'+((this.form.get('check_Date1').value)._i).month : ((this.form.get('check_Date1').value)._i).month;

    //   this.checkMonth1 = (this.checkMonth1<13) ? Number(this.checkMonth1)+Number(1) :  this.checkMonth1;

    //   this.checkMonth1 = (((this.checkMonth1).toString()).length!=2) ? 0+this.checkMonth1 : this.checkMonth1;

    //   this.check_DateData1 = ((this.form.get('check_Date1').value)._i).year+'-'+this.checkMonth1+'-'+((this.form.get('check_Date1').value)._i).date;

    // }else{
    //   this.check_DateData1 = this.form.get('check_Date1').value
    // }

    // this.check_DateData1 = (this.check_DateData1) ? this.check_DateData1 : '0';

    // if(((this.form.get('check_Date').value)._i)){
      
    //   this.checkMonth = (((this.form.get('check_Date').value)._i).month.length!=2) ? '0'+((this.form.get('check_Date').value)._i).month : ((this.form.get('check_Date').value)._i).month;

    //   this.checkMonth = (this.checkMonth<13) ? Number(this.checkMonth)+Number(1) :  this.checkMonth;

    //   this.checkMonth = (((this.checkMonth).toString()).length!=2) ? 0+this.checkMonth : this.checkMonth;

    //   this.check_DateData = ((this.form.get('check_Date').value)._i).year+'-'+this.checkMonth+'-'+((this.form.get('check_Date').value)._i).date;

    // }else{
    //   this.check_DateData = this.form.get('check_Date').value;
    // }

    

    // this.check_DateData = (this.check_DateData) ? this.check_DateData : '0';

   const status =  (this.form.get('type').value) ? this.form.get('type').value : '2';
   const processor_type =  (this.form.get('processor_type').value) ? this.form.get('processor_type').value : '0';
   const stEmail = (this.form.get('STEmail').value) ? this.form.get('STEmail').value : '0';
    
   if(status=="1"){

      this. sType = "Assessments";

   }else{

    this. sType = "Refunds";

   }

   if(stEmail==1){
     this.STEmail = true;
   }else{
    this.STEmail = false;
   }
    this.RefundService.getProcessorCount(this.check_DateData,status,this.check_DateData1,stEmail,processor_type).subscribe((data) => {
      this.isLoading = false;
      const ELEMENT_DATA = data['data'];
      this.dataSource = new MatTableDataSource(ELEMENT_DATA);
      this.dataSource.paginator = this.paginator;
       this.processorCount = data['count'];
    },
        error => {this.error = error}
      );


  }


  send(reportType){


    this.isLoading = true;
   
    // if(((this.form.get('check_Date1').value)._i)){
      
    //   this.checkMonth1 = (((this.form.get('check_Date1').value)._i).month.length!=2) ? '0'+((this.form.get('check_Date1').value)._i).month : ((this.form.get('check_Date1').value)._i).month;

    //   this.checkMonth1 = (this.checkMonth1<13) ? Number(this.checkMonth1)+Number(1) :  this.checkMonth1;

    //   this.checkMonth1 = (((this.checkMonth1).toString()).length!=2) ? 0+this.checkMonth1 : this.checkMonth1;

    //   this.check_DateData1 = ((this.form.get('check_Date1').value)._i).year+'-'+this.checkMonth1+'-'+((this.form.get('check_Date1').value)._i).date;

    // }else{
    //   this.check_DateData1 = this.form.get('check_Date1').value
    // }

    // this.check_DateData1 = (this.check_DateData1) ? this.check_DateData1 : '0';

    // if(((this.form.get('check_Date').value)._i)){
      
    //   this.checkMonth = (((this.form.get('check_Date').value)._i).month.length!=2) ? '0'+((this.form.get('check_Date').value)._i).month : ((this.form.get('check_Date').value)._i).month;

    //   this.checkMonth = (this.checkMonth<13) ? Number(this.checkMonth)+Number(1) :  this.checkMonth;

    //   this.checkMonth = (((this.checkMonth).toString()).length!=2) ? 0+this.checkMonth : this.checkMonth;

    //   this.check_DateData = ((this.form.get('check_Date').value)._i).year+'-'+this.checkMonth+'-'+((this.form.get('check_Date').value)._i).date;

    // }else{
    //   this.check_DateData = this.form.get('check_Date').value;
    // }

    

  //  this.check_DateData = (this.check_DateData) ? this.check_DateData : '0';

    if(this.processorCount=='0'){

      this.openSnackBar("Processer not found", 'Error') 
      this.isLoading = false;
    }else{
      const sReportType = (reportType) ? reportType : '1';

      const status =  (this.form.get('type').value) ? this.form.get('type').value : '2';

    if (confirm('You are sending reports to '+this.processorCount+' processors')) {

      const stEmail = (this.form.get('STEmail').value) ? this.form.get('STEmail').value : '2';

      const processor_type =  (this.form.get('processor_type').value) ? this.form.get('processor_type').value : '0';
 
      this.RefundService.getReport(this.check_DateData,status,this.check_DateData1,sReportType,stEmail,processor_type).subscribe((data) => {
        localStorage.setItem('sucessMsg','Report Send in progress');
        this.router.navigate(['/apps/admin/manage-reports']);
        },
          error => {this.error = error}
        );
      }else{

        this.filterChange();
      }
  }

  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }



}
