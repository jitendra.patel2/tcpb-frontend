import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {UsersService} from '../../../../service/users.service';

import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';
import {MatSnackBar} from '@angular/material/snack-bar';

import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-manage-profile',
  templateUrl: './manage-profile.component.html',
  styleUrls: ['./manage-profile.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})




export class ManageProfileComponent implements OnInit {



  displayedColumns: string[] = ['id', 'name', 'email','role','action'];
  dataSource = new MatTableDataSource();
  @ViewChild(MatPaginator) paginator: MatPaginator;


  @ViewChild('filter', {static: true})
  filter: ElementRef;

  constructor(
    
    private route: ActivatedRoute,
    private UsersService : UsersService,
    private _snackBar: MatSnackBar
    ) { }
  title = 'Profile Detail';
  successMsg = '';
  error = {};
  isLoading = false;
  adminData: any = [];
  ngOnInit(): void {
    this.isLoading = true;
    this.successMsg = localStorage.getItem('sucessMsg');
    this.title = 'Profile Detail';
    if(JSON.parse(localStorage.getItem('currentUser'))){
      this.adminData = JSON.parse(localStorage.getItem('currentUser'));
    }
  }
}
