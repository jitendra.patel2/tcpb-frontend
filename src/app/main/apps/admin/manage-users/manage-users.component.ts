import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {UsersService} from '../../../../service/users.service';

import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';
import {MatSnackBar} from '@angular/material/snack-bar';

import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';

@Component({
  selector: 'app-manage-users',
  templateUrl: './manage-users.component.html',
  styleUrls: ['./manage-users.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})




export class ManageUsersComponent implements OnInit {



  displayedColumns: string[] = ['id', 'name', 'email','role','action'];
  dataSource = new MatTableDataSource();
  @ViewChild(MatPaginator) paginator: MatPaginator;


  @ViewChild('filter', {static: true})
  filter: ElementRef;

  constructor(
    
    private route: ActivatedRoute,
    private UsersService : UsersService,
    private _snackBar: MatSnackBar
    ) { }
  title = 'Users Management';
  successMsg = '';
  error = {};
  isLoading = false;


  
  ngOnInit(): void {
    this.isLoading = true;
    this.successMsg = localStorage.getItem('sucessMsg');

    
    if(this.successMsg){
      this.openSnackBar(this.successMsg,'Success');
      localStorage.removeItem('sucessMsg');

    };


    this.UsersService.getUsers('0','0','0').subscribe((data) => {
      this.isLoading = false;
      const ELEMENT_DATA = data['data'];
      this.dataSource = new MatTableDataSource(ELEMENT_DATA);
      this.dataSource.paginator = this.paginator;
    },
      error => {this.error = error}
    );
  }




  onDelete(id: String) {
    if (confirm('Are you sure want to delete this ?')) {
  
      this.UsersService.updateStatusUser(id,2).subscribe(
        res => {
          localStorage.setItem('sucessMsg',"User deleted successfully.");
          this.ngOnInit();
        },
        error => this.error = error
      );
    }
  }

  2
3
  


public doFilter = (value: string) => {
  this.isLoading = true;
    const search = (value.trim().toLocaleLowerCase()) ? value.trim().toLocaleLowerCase() : '0';
    this.UsersService.getUsers('0','0',search).subscribe((data) => {
        this.isLoading = false;
        const ELEMENT_DATA = data['data'];
        this.dataSource = new MatTableDataSource(ELEMENT_DATA);
        this.dataSource.paginator = this.paginator;
      },
        error => {this.error = error}
      );

  }

  onStatusChange(id: String,status: Number) {

    this.UsersService.updateStatusUser(id,+status).subscribe(
      res => {
        localStorage.setItem('sucessMsg',res.message);
        this.ngOnInit();
      },
      error => this.error = error
    );
  
}


  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

}
