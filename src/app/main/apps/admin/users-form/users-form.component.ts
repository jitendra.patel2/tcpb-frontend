import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import {UsersService} from '../../../../service/users.service';
import {MatSnackBar} from '@angular/material/snack-bar';;


import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';

@Component({
  selector: 'app-users-form',
  templateUrl: './users-form.component.html',
  styleUrls: ['./users-form.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class UsersFormComponent implements OnInit {

  
    // Private
    private _unsubscribeAll: Subject<any>;
    /**
     * Constructor
     *
     * @param {FormBuilder} _formBuilder
     */
    constructor(
      private _formBuilder: FormBuilder,
      private UsersService : UsersService,
      private router: Router,
      private route: ActivatedRoute,
      private _snackBar: MatSnackBar
  )
  {
      // Set the private defaults
      this._unsubscribeAll = new Subject();
  }


  title = "Create User";
  form: FormGroup;
  countryData : [];
  stateData : [];
  roleData : [];
  error = '';
  formError = "";
  hidePw = true;
  ngOnInit(): void {

    this.UsersService.getCountry().subscribe((data) => {
      this.countryData = data['data'];

    });
    this.UsersService.getState().subscribe((data) => {
      this.stateData = data['data'];

    });


    this.UsersService.getRole().subscribe((data) => {
      this.roleData = data['data'];

    });

    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
      this.title = 'Edit User';
      this.hidePw = false;
  
      this.UsersService.getUser(id).subscribe( (res) => {
        const schools = res['data'];

          this.form.patchValue({
            firstName: schools.first_name,
            lastName: schools.last_name,
            email: schools.email,
            phone: schools.phone_number,
            password: schools.password,
            address: schools.address1,
            address2: schools.address2,
            city: schools.city,
            state: schools.state,
            postalCode: schools.zip,
            role: schools.user_role_id,
            id: schools.id
          });

        }
      );
    } else {
      this.hidePw = true;
      this.title = 'Create User';
    }

      // Reactive Form
      this.form = this._formBuilder.group({
        id : [''],
        firstName : ['', Validators.required],
        lastName  : ['', Validators.required],
        email  : ['', [Validators.required, Validators.maxLength(250),
          Validators.minLength(5),
  Validators.pattern(/.+@.+\..+/)]],
        phone  : ['',  [Validators.required,Validators.pattern(/^[0-9-+()]*$/), Validators.minLength(10), Validators.maxLength(10)]],
        password  : ['', [Validators.required, Validators.minLength(5), Validators.maxLength(100),Validators.pattern(/^(?=\D*\d)(?=[^a-z]*[a-z])(?=[^A-Z]*[A-Z]).{8,30}$/)]],
        address   : ['', Validators.required],
        address2  : [''],
        city      : ['', Validators.required],
        state     : ['', Validators.required],
        postalCode: ['', [Validators.required, Validators.maxLength(6), Validators.pattern("^[0-9]*$")]],
        role     : ['', Validators.required]
    });

    
  }

  public errorHandling = (control: string, error: string) => {
    return this.form.controls[control].hasError(error);
  }

  onSubmit () {


    const formData = new FormData();
    formData.append('firstName', this.form.get('firstName').value);
    formData.append('lastName', this.form.get('lastName').value);
    formData.append('email', this.form.get('email').value);
    formData.append('password', this.form.get('password').value);
    formData.append('phone', this.form.get('phone').value);
    formData.append('address', this.form.get('address').value);
    formData.append('address2', this.form.get('address2').value);
    formData.append('city', this.form.get('city').value);
    formData.append('state', this.form.get('state').value);
    formData.append('postalCode', this.form.get('postalCode').value);
    formData.append('role', this.form.get('role').value);

    const id = this.form.get('id').value;

    if (id) {
      this.UsersService.updateUser(formData,id).subscribe(
        res => {


          if (res.status === 'fail') {
            this.formError = res.message;
            this.openSnackBar(this.formError,'Error');
          } else {
            localStorage.setItem('sucessMsg',res.message);
            this.router.navigate(['/apps/admin/manage-users']);
          }
        },
        error => this.error = error
      );
    } else {
      this.UsersService.createUser(formData).subscribe(
        res => {
          if (res.status === 'fail') {
            this.formError = res.message;
            this.openSnackBar(this.formError,'Error');
          } else {
            localStorage.setItem('sucessMsg',res.message);
            this.router.navigate(['/apps/admin/manage-users']);
          }
        },
        error => this.error = error
      );
    }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }



}
