
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule, Routes } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatRippleModule } from '@angular/material/core';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSelectModule } from '@angular/material/select';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { MatTabsModule } from '@angular/material/tabs';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { AgmCoreModule } from '@agm/core';

import { FuseSharedModule } from '@fuse/shared.module';
import { FuseWidgetModule } from '@fuse/components/widget/widget.module';

import { AdminRoutingModule } from './admin-routing.module';
import { ManageProcessorsComponent } from './manage-processors/manage-processors.component';
import { ManageProducersComponent } from './manage-producers/manage-producers.component';
import { ManageUsersComponent } from './manage-users/manage-users.component';
import { ManageProfileComponent } from './manage-profile/manage-profile.component';
import { UsersFormComponent } from './users-form/users-form.component';
import { AdminFormComponent } from './admin-form/admin-form.component';
import { EmailFormComponent } from './email-form/email-form.component';
import { ProcessorsFormComponent } from './processors-form/processors-form.component';
import { ProducersFormComponent } from './producers-form/producers-form.component';
import { ManageRefundComponent } from './manage-refund/manage-refund.component';
import { ManageAissmentComponent } from './manage-aissment/manage-aissment.component';
import { AssessmentFormComponent } from './assessment-form/assessment-form.component';
import { RefundFormComponent } from './refund-form/refund-form.component';
import { ChangePasswordFormComponent } from './changePassword-form/changePassword-form.component';
import {MatDatepickerModule} from '@angular/material/datepicker'; 
import { FlexLayoutModule } from '@angular/flex-layout';
import { ManageNotesComponent } from './manage-notes/manage-notes.component';
import { NotesFormComponent } from './notes-form/notes-form.component';
import { NgxMatSelectSearchModule } from 'ngx-mat-select-search';
import {MatCardModule} from '@angular/material/card';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import { ManageViewAssessmentsComponent } from './manage-view-assessments/manage-view-assessments.component';
import { ManageViewRefundsComponent } from './manage-view-refunds/manage-view-refunds.component'; 
import { MatDialogModule } from '@angular/material/dialog';
import { ModelComponent } from './model/model.component';
import { TextEditorComponent } from './model/texteditor.component';

import { ManageReportComponent } from './manage-report/manage-report.component';
import { SavequeryModelComponent } from './savequery-model/savequery-model.component';
import { ManageSavequeryComponent } from './manage-savequery/manage-savequery.component';
import { ManageProcessorReportComponent } from './manage-processor-report/manage-processor-report.component';
import { ManageReportSendProcessorComponent } from './manage-report-send-processor/manage-report-send-processor.component';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import { HttpClientModule} from '@angular/common/http';
import { AngularEditorModule } from '@kolkov/angular-editor';
@NgModule({
  declarations: [ManageProcessorsComponent, ManageProducersComponent, ManageUsersComponent,ManageProfileComponent, UsersFormComponent,AdminFormComponent,EmailFormComponent, ProcessorsFormComponent, ProducersFormComponent, ManageRefundComponent, ManageAissmentComponent, AssessmentFormComponent, RefundFormComponent,ChangePasswordFormComponent, ManageNotesComponent, NotesFormComponent, ManageViewAssessmentsComponent, ManageViewRefundsComponent, ModelComponent,TextEditorComponent, ManageReportComponent, SavequeryModelComponent, ManageSavequeryComponent, ManageProcessorReportComponent, ManageReportSendProcessorComponent],
  imports: [
    CommonModule,
    MatButtonModule,
    MatChipsModule,
    MatExpansionModule,
    MatFormFieldModule,
    MatIconModule,
    MatInputModule,
    MatPaginatorModule,
    MatRippleModule,
    MatSelectModule,
    MatSortModule,
    MatSnackBarModule,
    MatTableModule,
    MatDatepickerModule,
    MatCardModule,
    MatProgressSpinnerModule,
    NgxMatSelectSearchModule,
    MatTabsModule,
    FlexLayoutModule,
    MatDialogModule,
    NgxChartsModule,
    MatSlideToggleModule,
    HttpClientModule,
    AngularEditorModule,
    AgmCoreModule.forRoot({
        apiKey: 'AIzaSyD81ecsCj4yYpcXSLFcYU97PvRsE_X8Bx8'
    }),

    FuseSharedModule,
    FuseWidgetModule,
    AdminRoutingModule
  ]
})
export class AdminModule { }
