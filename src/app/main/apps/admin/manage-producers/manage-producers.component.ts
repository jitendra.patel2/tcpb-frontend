import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {ProducersService} from '../../../../service/producers.service';
import {MatSnackBar} from '@angular/material/snack-bar';

import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';

import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';

import { ExportToCsv } from 'export-to-csv';

@Component({
selector: 'app-manage-producers',
  templateUrl: './manage-producers.component.html',
  styleUrls: ['./manage-producers.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class ManageProducersComponent implements OnInit {

  displayedColumns: string[] = ['producer_id', 'name', 'city','address1','activeMonth','action'];
  dataSource = new MatTableDataSource();
 @ViewChild(MatPaginator) paginator: MatPaginator;


  constructor(
    
    private route: ActivatedRoute,
    private ProducersService : ProducersService,
    private _snackBar: MatSnackBar
    ) { }
    title = 'Producers Management';
  successMsg = '';
  error = {};
  exportData = [];
  isLoading = false;
  ngOnInit(): void {

    this.isLoading = true;

    this.successMsg = localStorage.getItem('sucessMsg');

    if(this.successMsg){
      this.openSnackBar(this.successMsg,'Success');
      localStorage.removeItem('sucessMsg');

    }



    this.ProducersService.getProducers('0','0','0').subscribe((data) => {
      const ELEMENT_DATA = data['data'];
      this.exportData  = data['data'];
      this.dataSource = new MatTableDataSource(ELEMENT_DATA);
      this.dataSource.paginator = this.paginator;
      this.isLoading = false;
    },
      error => {this.error = error}
    );


  }

  public doFilter = (value: string) => {
    this.isLoading = true;
    const search = (value.trim().toLocaleLowerCase()) ? (value.trim().toLocaleLowerCase()).toString() : '0';
    this.ProducersService.getProducers('0','0',search).subscribe((data) => {
        const ELEMENT_DATA = data['data'];
        this.exportData  = data['data'];
        this.dataSource = new MatTableDataSource(ELEMENT_DATA);
        this.dataSource.paginator = this.paginator;
        this.isLoading = false;
      },
        error => {this.error = error}
      );
  
  }
  
  onDelete(id: String) {
    if (confirm('Are you sure want to delete this ?')) {
  
      this.ProducersService.updateStatusProducer(id,2).subscribe(
        res => {
          localStorage.setItem('sucessMsg',"Producer deleted successfully.");
          this.ngOnInit();
        },
        error => this.error = error
      );
    }
  }


  onStatusChange(id: String,status: Number) {

    this.ProducersService.updateStatusProducer(id,+status).subscribe(
      res => {
        localStorage.setItem('sucessMsg',res.message);
        this.ngOnInit();
      },
      error => this.error = error
    );
  
}

exportCsv() {
  // this.StudentService.getStudent(id).subscribe( (res) => {
  //     const csvExporter = new ExportToCsv(res['data']);
  //     csvExporter.generateCsv(res['data']);
  //   }
  // );



  var data = this.exportData;
  for(let i in data){
  if(data[i].status == 1) {
      data[i].status = 'Active';
    } else {
      data[i].status = 'Inactive';
    }
  }

  this.exportData = this.exportData.filter(item => item.status !== status); 
  
    const options = { 
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalSeparator: '.',
      showLabels: true, 
      showTitle: true,
      title: 'Producers List',
      useTextFile: false,
      useBom: true,
      useKeysAsHeaders: true,
      // headers: ['Column 1', 'Column 2', etc...] <-- Won't work with useKeysAsHeaders present!
    };
   
  const csvExporter = new ExportToCsv(options);
   
  csvExporter.generateCsv(data);


}

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

}
