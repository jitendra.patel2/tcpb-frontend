import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {RefundService} from '../../../../service/refund.service'
import {UsersService} from '../../../../service/users.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';

import { ExportToCsv } from 'export-to-csv';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {SavequeryModelComponent} from '../savequery-model/savequery-model.component';
import { HttpClient, HttpErrorResponse,HttpHeaders} from '@angular/common/http';

import * as JSZip from 'jszip';  
import * as FileSaver from 'file-saver';  
@Component({
  selector: 'app-manage-report',
  templateUrl: './manage-report.component.html',
  styleUrls: ['./manage-report.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class ManageReportComponent implements OnInit {

  // Private
  // Private
/**
 * Constructor
 *
 * @param {FormBuilder} _formBuilder
 */
displayedColumns: string[] = ['id','processor_count', 'status','date','action'];
dataSource = new MatTableDataSource();
@ViewChild(MatPaginator) paginator: MatPaginator;


constructor(
  private _formBuilder: FormBuilder,
  private route: ActivatedRoute,
  private RefundService : RefundService,
  private UsersService : UsersService,
  private _snackBar: MatSnackBar,
  public dialog: MatDialog,
  private http: HttpClient
  ) { }
  title = 'Report Management';
  successMsg = '';
  error = {};
  form: FormGroup;
  processorData : [];
producerData : [];
checkMonth = 0;
checkMonth1 = 0;
check_DateData = '0';
check_DateData1 = '0';
processorCount = '0';
isLoading = false;
exportData = [];
files = [];
c = 1;  
  ngOnInit(): void {

    this.isLoading = true;

    
    this.successMsg = localStorage.getItem('sucessMsg');

    if(this.successMsg){
      this.openSnackBar(this.successMsg,'Success');
      localStorage.removeItem('sucessMsg');

    }
    localStorage.removeItem('processor_id');
    localStorage.removeItem('producer_Id');

    // Reactive Form
    this.form = this._formBuilder.group({
      processor_id : ['',Validators.required],
      check_Date  : [''],
      check_Date1 : ['']
    });

      this.RefundService.getReportList().subscribe((data) => {

        this.isLoading = false;
        const ELEMENT_DATA = data['data'];
        this.exportData = data['data'];
        this.dataSource = new MatTableDataSource(ELEMENT_DATA);
        this.dataSource.paginator = this.paginator;
      },
        error => {this.error = error}
      );
    
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

  async getFile(url: string) {  
    const httpOptions = {  
      responseType: 'blob' as 'json'  
    };  
    const res = await this.http.get(url, httpOptions).toPromise().catch((err: HttpErrorResponse) => {  
      const error = err.error;  
      return error;  
    });  
    return res;  
  }  


  async download(id){

    console.log(id);
    this.isLoading = true;

    this.RefundService.getReportListById(id,'reports').subscribe((data) => {

      const ELEMENT_DATA = data['data'];
      this.createZip(ELEMENT_DATA);
   
    });

   
  }

  async createZip(files: any[]) {  

    var zip = new JSZip();
    this.c = 1;

      for (let counter = 0; counter < files.length; counter++) {  
      const element = files[counter]['url'];  
      console.log(element);
      const fileData: any = await this.getFile(element);  

      const b: any = new Blob([fileData], { type: '' + fileData.type + '' }); 

      zip.file(element.substring(element.lastIndexOf('/') + 1), b);  
      console.log(files.length);
      console.log(this.c);
        
      if(this.c==files.length){

        zip.generateAsync({type:"blob"})
        .then(function(content) {
            if(content){
              console.log('bbbb');
              FileSaver.saveAs(content, "report.zip");
             
            }
        });
        setTimeout (() => {
          this.isLoading = false;
       }, 3000);
      }
      this.c++;
    } 
  }

}
