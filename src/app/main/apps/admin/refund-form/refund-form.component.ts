import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import {RefundService} from '../../../../service/refund.service';
import {UsersService} from '../../../../service/users.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import * as moment from 'moment';
import 'moment/locale/pt-br';
import { FormControl } from '@angular/forms';
import { fuseAnimations } from '@fuse/animations';
import { takeUntil } from 'rxjs/operators';
@Component({
  selector: 'app-refund-form',
  templateUrl: './refund-form.component.html',
  styleUrls: ['./refund-form.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class RefundFormComponent implements OnInit {


// Private
private _unsubscribeAll: Subject<any>;

  public processor_id: FormControl = new FormControl();

  public producer_id: FormControl = new FormControl();

  public processorFilterCtrl: FormControl = new FormControl();

  public producerFilterCtrl : FormControl = new FormControl();


  public filteredprocessor = [];

  public filteredproducer = [];
    
  protected _onDestroy = new Subject<void>(); 
/**
 * Constructor
 *
 * @param {FormBuilder} _formBuilder
 */
constructor(
  private _formBuilder: FormBuilder,
  private RefundService : RefundService,
  private UsersService : UsersService,
  private router: Router,
  private _snackBar: MatSnackBar,
  private route: ActivatedRoute
)
{
  // Set the private defaults
  this._unsubscribeAll = new Subject();
}


title = "Create Refund";
form: FormGroup;
processorData : [];
producerData : [];
accessTypeData = [
  {
    'id':1,
    'title' : 'Grain'
  },
  {
    'id':2,
    'title' : 'Silage'
  }
];
error = '';
formError = "";
process_DateData = "";
entry_DateData = "";
check_DateData = "";
month = 0;
checkMonth = 0;
  ngOnInit(): void {


    this.UsersService.getProcessors().subscribe((data) => {
      this.processorData = data['data'];
      this.filteredprocessor = this.processorData;
    });


    this.UsersService.getProducers().subscribe((data) => {
      this.producerData = data['data'];
      this.filteredproducer = this.producerData;

    });

    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
      this.title = 'Edit Refund';

  
      this.RefundService.getRefund(id).subscribe( (res) => {
        const schools = res['data'];
          //  let new_process_Date = schools.process_Date ? new Date(schools.process_Date):"";
          //  let new_entry_Date = schools.entry_Date ? new Date(schools.entry_Date):"";
          let new_process_Date = schools.process_Date ? schools.process_Date:"";
          let new_entry_Date = schools.entry_Date ? schools.entry_Date:"";
            this.form.patchValue({
              processor_id: Number(schools.processor_id),
              producer_id: Number(schools.producer_id),
              check_Date: schools.check_Date,
              check_Num: schools.check_Num,
              amount: schools.amount,
              buTons: schools.buTons,
              accessType: Number(schools.acssessType),
              process_Date: new_process_Date,
              entry_Date: new_entry_Date,
              notes: schools.notes,
              id: schools.id
            });
        }
      );
    } else {
      this.title = 'Create Refund';
    }



     // Reactive Form
     this.form = this._formBuilder.group({
      id : [''],
      processor_id : ['', Validators.required],
      producer_id : ['', Validators.required],
      check_Date   : [''],
      check_Num  : [''],
      amount      : ['',[ Validators.required,Validators.pattern(/^[0-9]\d*(\.\d+)?$/)]],
      buTons     : ['', [Validators.required, Validators.pattern(/^[0-9]\d*(\.\d+)?$/)]],
      accessType     : [1, Validators.required],
      process_Date     : [''],
      entry_Date     : [''],
      notes : [''],
  });

  this.processorFilterCtrl.valueChanges
  .pipe(takeUntil(this._onDestroy))
  .subscribe(() => {
    this.filterProcessor();
  });

  this.producerFilterCtrl.valueChanges
  .pipe(takeUntil(this._onDestroy))
  .subscribe(() => {
    this.filterProducer();
  });

  }

  protected filterProcessor() {
    if (!this.processorData) {
      return;
    }
    let search = this.processorFilterCtrl.value;
    this.filteredprocessor = this.processorData.filter((element : any) => element.company.toLowerCase().indexOf(search) > -1 )
  }

  protected filterProducer() {
    if (!this.processorData) {
      return;
    }
    let search = this.producerFilterCtrl.value;
    this.filteredproducer = this.producerData.filter((element : any) => element.firstName.toLowerCase().indexOf(search) > -1 )
  }

  public errorHandling = (control: string, error: string) => {
    return this.form.controls[control].hasError(error);
  }

  butonsFormula(value: Number){

    const id = this.route.snapshot.paramMap.get('id');
    if (id) {}else{

    const accessType = this.form.get('accessType').value;
    if(accessType==1){
      
      this.form.patchValue({
        amount:  (Number(value)*0.01).toFixed(0)  
      });

    }else{

      this.form.patchValue({
        amount:  (Number(value)*0.074).toFixed(0) 
      });
    }
  
  }


  }

  onSubmit () {
    if(((this.form.get('process_Date').value)._i)){
      if(((this.form.get('process_Date').value)._i).month && ((this.form.get('process_Date').value)._i).month.length){
        let processLength = ((this.form.get('process_Date').value)._i).month.length;
      this.month = (processLength!=2) ? '0'+((this.form.get('process_Date').value)._i).month : ((this.form.get('process_Date').value)._i).month;
      this.month = (this.month<13) ? Number(this.month)+Number(1) :  this.month;
      this.month = (((this.month).toString()).length!=2) ? 0+this.month : this.month;
      this.process_DateData = ((this.form.get('process_Date').value)._i).year+'-'+this.month+'-'+((this.form.get('process_Date').value)._i).date;
    } else {
      if(((this.form.get('process_Date').value)._d)){
        let new_date = ((this.form.get('process_Date').value)._d)
        this.process_DateData = moment(new_date).format('YYYY-MM-DD')
      }
      }
    }else{
      this.process_DateData = this.form.get('process_Date').value
    }

    if(((this.form.get('entry_Date').value)._i)){
      if(((this.form.get('entry_Date').value)._i).month && ((this.form.get('entry_Date').value)._i).month.length){
        let processLength = ((this.form.get('entry_Date').value)._i).month.length;
      this.month = (processLength!=2) ? '0'+((this.form.get('entry_Date').value)._i).month : ((this.form.get('entry_Date').value)._i).month;
      this.month = (this.month<13) ? Number(this.month)+Number(1) :  this.month;
      this.month = (((this.month).toString()).length!=2) ? 0+this.month : this.month;
      this.entry_DateData = ((this.form.get('entry_Date').value)._i).year+'-'+this.month+'-'+((this.form.get('entry_Date').value)._i).date;
    } else {
      if(((this.form.get('entry_Date').value)._d)){
        let new_date = ((this.form.get('entry_Date').value)._d)
        this.entry_DateData = moment(new_date).format('YYYY-MM-DD')
      }
      }
    }else{
      this.entry_DateData = this.form.get('entry_Date').value
    }

    if(((this.form.get('check_Date').value)._i)){
      
      this.checkMonth = (((this.form.get('check_Date').value)._i).month.length!=2) ? '0'+((this.form.get('check_Date').value)._i).month : ((this.form.get('check_Date').value)._i).month;

      this.checkMonth = (this.checkMonth<13) ? Number(this.checkMonth)+Number(1) :  this.checkMonth;

      this.checkMonth = (((this.checkMonth).toString()).length!=2) ? 0+this.checkMonth : this.checkMonth;

      this.check_DateData = ((this.form.get('check_Date').value)._i).year+'-'+this.checkMonth+'-'+((this.form.get('check_Date').value)._i).date;

    }else{
      this.check_DateData = this.form.get('check_Date').value
    }
    
    const formData = new FormData();
    formData.append('processor_id', this.form.get('processor_id').value);
    formData.append('producer_id', this.form.get('producer_id').value);
    formData.append('check_Date', this.check_DateData);
    formData.append('check_Num', this.form.get('check_Num').value);
    formData.append('amount', this.form.get('amount').value);
    formData.append('buTons', this.form.get('buTons').value);
    formData.append('acssessType', this.form.get('accessType').value);
    formData.append('notes', this.form.get('notes').value);
    formData.append('process_Date', this.process_DateData);
    formData.append('entry_Date', this.entry_DateData);
    formData.append('created_by','');
    formData.append('updated_by','');
    const id = this.form.get('id').value;

    if (id) {
      this.RefundService.updateRefund(formData,id).subscribe(
        res => {


          if (res.status === 'fail') {
            this.formError = res.message;
            this.openSnackBar(this.formError,'Error');
          } else {
            localStorage.setItem('sucessMsg',res.message);
            this.router.navigate(['/apps/admin/manage-refund']);
          }
        },
        error => this.error = error
      );
    } else {
      this.RefundService.createRefund(formData).subscribe(
        res => {
          if (res.status === 'fail') {
            this.formError = res.message;
            this.openSnackBar(this.formError,'Error');
          } else {
            localStorage.setItem('sucessMsg',res.message);
            this.router.navigate(['/apps/admin/manage-refund']);
          }
        },
        error => this.error = error
      );
    }
  }


  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }


}
