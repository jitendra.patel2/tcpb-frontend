import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {RefundService} from '../../../../service/refund.service'
import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';

import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-manage-savequery',
  templateUrl: './manage-savequery.component.html',
  styleUrls: ['./manage-savequery.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class ManageSavequeryComponent implements OnInit {

  displayedColumns: string[] = ['id','topic','action'];
dataSource = new MatTableDataSource();
@ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private route: ActivatedRoute,
    private RefundService : RefundService,
    private _snackBar: MatSnackBar
    ) { }

    title = 'Query Management';
    successMsg = '';
    error = {};
  isLoading = false;
  ngOnInit(): void {

    this.successMsg = localStorage.getItem('sucessMsg');

    if(this.successMsg){
      this.openSnackBar(this.successMsg,'Success');
      localStorage.removeItem('sucessMsg');

    }

    this.isLoading = true;
    this.RefundService.listQuery().subscribe((data) => {
      const ELEMENT_DATA = data['data'];
      this.dataSource = new MatTableDataSource(ELEMENT_DATA);
      this.dataSource.paginator = this.paginator;
      this.isLoading = false;  
    },
      error => {this.error = error}
    );

  }

  runQuery(id: String) {

    this.RefundService.getQueryById(id).subscribe(
      res => {
        const data = res['data'];

        this.RefundService.getReport(data.check_Date,data.status,data.check_DateData1,'1','0','0').subscribe((data) => {
          localStorage.setItem('sucessMsg','Report Send in progress');
            this.ngOnInit();
          },
            error => {this.error = error}
          );
      },
      error => this.error = error
    );
  
}

openSnackBar(message: string, action: string) {
  this._snackBar.open(message, action, {
    duration: 2000,
  });
}

}
