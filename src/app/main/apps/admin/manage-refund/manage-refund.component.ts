import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {RefundService} from '../../../../service/refund.service'
import {UsersService} from '../../../../service/users.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import * as moment from 'moment';
import 'moment/locale/pt-br';
import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';

import { ExportToCsv } from 'export-to-csv';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSnackBar} from '@angular/material/snack-bar';

@Component({
  selector: 'app-manage-refund',
  templateUrl: './manage-refund.component.html',
  styleUrls: ['./manage-refund.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class ManageRefundComponent implements OnInit {

  // Private
/**
 * Constructor
 *
 * @param {FormBuilder} _formBuilder
 */
displayedColumns: string[] = ['id','producer_id', 'processor_id', 'buTons','amount','action'];
dataSource = new MatTableDataSource();
@ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
    private _formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private RefundService : RefundService,
    private UsersService : UsersService,
    private _snackBar: MatSnackBar
    ) { }
    title = 'Refunds Management';
  successMsg = '';
  error = {};
  form: FormGroup;
  processorData : [];
producerData : [];
checkMonth = 0;
checkMonth1 = 0;
check_DateData = '0';
check_DateData1 = '0';
isLoading = false;
exportData = [];

  ngOnInit(): void {
   this.isLoading = true;

    this.UsersService.getProcessors().subscribe((data) => {
      this.processorData = data['data'];

    });


    this.UsersService.getProducers().subscribe((data) => {
      this.producerData = data['data'];

    });

    this.successMsg = localStorage.getItem('sucessMsg');

    
    this.successMsg = localStorage.getItem('sucessMsg');

    if(this.successMsg){
      this.openSnackBar(this.successMsg,'Success');
      localStorage.removeItem('sucessMsg');

    }
    localStorage.removeItem('processor_id');
    localStorage.removeItem('producer_Id');

 // Reactive Form
 this.form = this._formBuilder.group({
  processor_id : [''],
  producer_id  : [''],
  search : [''],
  check_Date  : [''],
  check_Date1 : [''],
  status : ['1']
});
    this.RefundService.getRefunds('0','0','0','0','0','0').subscribe((data) => {

      this.isLoading = false;
      const ELEMENT_DATA = data['data'];
      this.exportData = data['data'];
      this.dataSource = new MatTableDataSource(ELEMENT_DATA);
      this.dataSource.paginator = this.paginator;

  
    },
      error => {this.error = error}
    );
  }


  


  filterChange(value: String,event: String,producer_id:String){

    this.isLoading = true;
    const search = (this.form.get('search').value) ? (this.form.get('search').value).trim().toLocaleLowerCase() : '0';

    // if(((this.form.get('check_Date1').value)._i)){
      
    //   this.checkMonth1 = (((this.form.get('check_Date1').value)._i).month.length!=2) ? '0'+((this.form.get('check_Date1').value)._i).month : ((this.form.get('check_Date1').value)._i).month;

    //   this.checkMonth1 = (this.checkMonth1<13) ? Number(this.checkMonth1)+Number(1) :  this.checkMonth1;

    //   this.checkMonth1 = (((this.checkMonth1).toString()).length!=2) ? 0+this.checkMonth1 : this.checkMonth1;

    //   this.check_DateData1 = ((this.form.get('check_Date1').value)._i).year+'-'+this.checkMonth1+'-'+((this.form.get('check_Date1').value)._i).date;
    // }else{
    //   this.check_DateData1 = this.form.get('check_Date1').value
    // }
    if(((this.form.get('check_Date1').value)._d)){
      let new_date = ((this.form.get('check_Date1').value)._d)
      this.check_DateData1 = moment(new_date).format('YYYY-MM-DD')
    }
    if(((this.form.get('check_Date').value)._d)){
      let new_date = ((this.form.get('check_Date').value)._d)
      this.check_DateData = moment(new_date).format('YYYY-MM-DD')
    }
    this.check_DateData1 = (this.check_DateData1) ? this.check_DateData1 : '0';

    // if(((this.form.get('check_Date').value)._i)){
      
    //   this.checkMonth = (((this.form.get('check_Date').value)._i).month.length!=2) ? '0'+((this.form.get('check_Date').value)._i).month : ((this.form.get('check_Date').value)._i).month;

    //   this.checkMonth = (this.checkMonth<13) ? Number(this.checkMonth)+Number(1) :  this.checkMonth;

    //   this.checkMonth = (((this.checkMonth).toString()).length!=2) ? 0+this.checkMonth : this.checkMonth;

    //   this.check_DateData = ((this.form.get('check_Date').value)._i).year+'-'+this.checkMonth+'-'+((this.form.get('check_Date').value)._i).date;
   
    // }else{
    //   this.check_DateData = this.form.get('check_Date').value;
    // }

    

    this.check_DateData = (this.check_DateData) ? this.check_DateData : '0';

   const status = (this.form.get('status').value) ? this.form.get('status').value : '0';
   
   const processorD_id = (this.form.get('processor_id').value) ? (this.form.get('processor_id').value) : '0';
   const producerD_id = (this.form.get('producer_id').value) ? (this.form.get('producer_id').value) : '0';


   this.RefundService.getRefunds(processorD_id,producerD_id,this.check_DateData,search,status,this.check_DateData1).subscribe((data) => {
    this.isLoading = false;
    this.exportData = data['data'];
    const ELEMENT_DATA = data['data'];
    this.dataSource = new MatTableDataSource(ELEMENT_DATA);
    this.dataSource.paginator = this.paginator;

  
    },
      error => {this.error = error}
    );
  
  }


  exportCsv(value: String,event: String,producer_id:String) {

    this.isLoading = true;
    const search = (this.form.get('search').value) ? (this.form.get('search').value).trim().toLocaleLowerCase() : '0';
    
    if(((this.form.get('check_Date1').value)._i)){
      
      this.checkMonth1 = (((this.form.get('check_Date1').value)._i).month.length!=2) ? '0'+((this.form.get('check_Date1').value)._i).month : ((this.form.get('check_Date1').value)._i).month;

      this.checkMonth1 = (this.checkMonth1<13) ? Number(this.checkMonth1)+Number(1) :  this.checkMonth1;

      this.checkMonth1 = (((this.checkMonth1).toString()).length!=2) ? 0+this.checkMonth1 : this.checkMonth1;

      this.check_DateData1 = ((this.form.get('check_Date1').value)._i).year+'-'+this.checkMonth1+'-'+((this.form.get('check_Date1').value)._i).date;

    }else{
      this.check_DateData1 = this.form.get('check_Date1').value
    }

    this.check_DateData1 = (this.check_DateData1) ? this.check_DateData1 : '0';

    if(((this.form.get('check_Date').value)._i)){
      
      this.checkMonth = (((this.form.get('check_Date').value)._i).month.length!=2) ? '0'+((this.form.get('check_Date').value)._i).month : ((this.form.get('check_Date').value)._i).month;

      this.checkMonth = (this.checkMonth<13) ? Number(this.checkMonth)+Number(1) :  this.checkMonth;

      this.checkMonth = (((this.checkMonth).toString()).length!=2) ? 0+this.checkMonth : this.checkMonth;

      this.check_DateData = ((this.form.get('check_Date').value)._i).year+'-'+this.checkMonth+'-'+((this.form.get('check_Date').value)._i).date;

    }else{
      this.check_DateData = this.form.get('check_Date').value;
    }

    

    this.check_DateData = (this.check_DateData) ? this.check_DateData : '0';


   const status = (this.form.get('status').value) ? this.form.get('status').value : '0';
   
   const processorD_id = (this.form.get('processor_id').value) ? (this.form.get('processor_id').value) : '0';
   const producerD_id = (this.form.get('producer_id').value) ? (this.form.get('producer_id').value) : '0';


   this.RefundService.getRefunds1(processorD_id,producerD_id,this.check_DateData,search,status,this.check_DateData1).subscribe((data) => {
        this.isLoading = false;
        window.open(data['url'], "_blank");  
    },
      error => {this.error = error}
    );
    // // this.StudentService.getStudent(id).subscribe( (res) => {
    // //     const csvExporter = new ExportToCsv(res['data']);
    // //     csvExporter.generateCsv(res['data']);
    // //   }
    // // );

    // var data = this.exportData;
     
    //   const options = { 
    //     fieldSeparator: ',',
    //     quoteStrings: '"',
    //     decimalSeparator: '.',
    //     showLabels: true, 
    //     showTitle: true,
    //     title: 'Refunds List',
    //     useTextFile: false,
    //     useBom: true,
    //     useKeysAsHeaders: true,
    //     // headers: ['Column 1', 'Column 2', etc...] <-- Won't work with useKeysAsHeaders present!
    //   };
     
    // const csvExporter = new ExportToCsv(options);
     
    // csvExporter.generateCsv(data);
 
  
  }

  onDelete(id: String) {
    if (confirm('Are you sure want to delete this ?')) {
  
      this.RefundService.updateStatusRefund(id,2).subscribe(
        res => {
          localStorage.setItem('sucessMsg',"Processor deleted successfully.");
          this.ngOnInit();
        },
        error => this.error = error
      );
    }
  }

  onStatusChange(id: String,status: Number) {

    this.RefundService.updateStatusRefund(id,+status).subscribe(
      res => {
        localStorage.setItem('sucessMsg',res.message);
        this.ngOnInit();
      },
      error => this.error = error
    );
  
}

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

}
