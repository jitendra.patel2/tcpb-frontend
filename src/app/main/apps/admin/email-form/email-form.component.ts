import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import {UsersService} from '../../../../service/users.service';
import {MatSnackBar} from '@angular/material/snack-bar';;
import {ProcessorsService} from '../../../../service/processors.service';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';

@Component({
  selector: 'app-email-form',
  templateUrl: './email-form.component.html',
  styleUrls: ['./email-form.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class EmailFormComponent implements OnInit {

  
    // Private
    private _unsubscribeAll: Subject<any>;
    /**
     * Constructor
     *
     * @param {FormBuilder} _formBuilder
     */
    constructor(
      private _formBuilder: FormBuilder,
      private UsersService : UsersService,
      private router: Router,
      private route: ActivatedRoute,
      private _snackBar: MatSnackBar,
      private ProcessorsService : ProcessorsService,
  )
  {
      // Set the private defaults
      this._unsubscribeAll = new Subject();
  }


  title = "Send Email";
  form: FormGroup;
  countryData : [];
  stateData : [];
  roleData : [];
  error = '';
  formError = "";
  hidePw = true;
  month = 0;
checkMonth = 0;
dateMonth = 0;
send_Date = '';
displayDateInput = false;

  editorConfig: AngularEditorConfig = {
    editable: true,
      spellcheck: true,
      height: 'auto',
      minHeight: '0',
      maxHeight: 'auto',
      width: 'auto',
      minWidth: '0',
      translate: 'yes',
      enableToolbar: true,
      showToolbar: true,
      placeholder: 'Enter text here...',
      defaultParagraphSeparator: '',
      // defaultFontName: '',
      // defaultFontSize: '',
      fonts: [
        {class: 'arial', name: 'Arial'},
        {class: 'times-new-roman', name: 'Times New Roman'},
        {class: 'calibri', name: 'Calibri'},
        {class: 'comic-sans-ms', name: 'Comic Sans MS'}
      ],
      customClasses: [
      {
        name: 'quote',
        class: 'quote',
      },
      {
        name: 'redText',
        class: 'redText'
      },
      {
        name: 'titleText',
        class: 'titleText',
        tag: 'h1',
      },
    ],
    uploadUrl: 'v1/image',
    uploadWithCredentials: false,
    sanitize: true,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [
      ['bold', 'italic'],
      ['fontSize']
    ]
  };
  ngOnInit(): void {

      // Reactive Form
      this.form = this._formBuilder.group({
      email      : ['', Validators.required],
      sendDate:['']
   });

   }

  public errorHandling = (control: string, error: string) => {
    return this.form.controls[control].hasError(error);
  }

  onSubmit () {

   if(((this.form.get('sendDate').value)._i)){
    if(((this.form.get('sendDate').value)._i).month && ((this.form.get('sendDate').value)._i).month.length){
     let monthLength = ((this.form.get('sendDate').value)._i).month.length;
     this.checkMonth = (monthLength!=2) ? '0'+((this.form.get('sendDate').value)._i).month : ((this.form.get('sendDate').value)._i).month;

   this.checkMonth = (this.checkMonth<13) ? Number(this.checkMonth)+Number(1) :  this.checkMonth;

   this.checkMonth = (((this.checkMonth).toString()).length!=2) ? 0+this.checkMonth : this.checkMonth;

   this.send_Date = ((this.form.get('sendDate').value)._i).year+'-'+this.checkMonth+'-'+((this.form.get('sendDate').value)._i).date;
    } else {
     this.send_Date = this.form.get('sendDate').value
    }
 }else{
   this.send_Date = this.form.get('sendDate').value
 }

 const formData = new FormData();
 formData.append('email', this.form.get('email').value);
 formData.append('sendDate', this.send_Date);
      this.ProcessorsService.sendEmail(formData).subscribe(
        res => {
         this.openSnackBar(res.message,'Success');
          if (res.status === 'fail') {
            this.formError = res.message;
            this.openSnackBar(this.formError,'Error');
          } else {
            localStorage.setItem('sucessMsg',res.message);

          }
        },
        error => this.error = error
      );
    
  }

  autoSend() {
 this.displayDateInput = true;
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }



}
