import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import {ProducersService} from '../../../../service/producers.service';
import {UsersService} from '../../../../service/users.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import { FormControl } from '@angular/forms';
import { fuseAnimations } from '@fuse/animations';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-producers-form',
  templateUrl: './producers-form.component.html',
  styleUrls: ['./producers-form.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class ProducersFormComponent implements OnInit {

 
   // Private
   private _unsubscribeAll: Subject<any>;

   public state: FormControl = new FormControl();
   public county_Num : FormControl = new FormControl();
   public filteredState = [];
   public filteredCounty = [];
  
   protected _onDestroy = new Subject<void>(); 
  public stateFilterCtrl: FormControl = new FormControl();

  public countyFilterCtrl :FormControl = new FormControl();
   /**
    * Constructor
    *
    * @param {FormBuilder} _formBuilder
    */
   constructor(
     private _formBuilder: FormBuilder,
     private ProducersService : ProducersService,
     private UsersService : UsersService,
     private router: Router,
     private _snackBar: MatSnackBar,
     private route: ActivatedRoute
 )
 {
     // Set the private defaults
     this._unsubscribeAll = new Subject();
 }

 title = "Create Producers";
 form: FormGroup;
 countryData : [];
 roleData : [];
 stateData : [];

 error = '';
 formError = "";

 monthData = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];

  ngOnInit(): void {

    this.UsersService.getCountry().subscribe((data) => {
      this.countryData = data['data'];
      this.filteredCounty = this.countryData;

    });

    this.UsersService.getState().subscribe((data) => {
      this.stateData = data['data'];
      this.filteredState = this.stateData;

    });
    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
      this.title = 'Edit Producer';

  
      this.ProducersService.getProducer(id).subscribe( (res) => {


        const schools = res['data'];
        this.title = 'Edit '+schools.firstName+' '+schools.lastName;

          this.form.patchValue({
            firstName: schools.firstName,
            lastName: schools.lastName,
            email: schools.email,
            phone: schools.phone,
            address: schools.address1,
            address2: schools.address2,
            city: schools.city,
            state: schools.state,
            postalCode: schools.zip,
            county_Num:schools.county_Num,
            startMonth: schools.startMonth,
            endMonth: schools.endMonth,
            id: schools.producer_id
          });

        }
      );
    } else {
      this.title = 'Create Producer';
    }

     // Reactive Form
     this.form = this._formBuilder.group({
      id : [''],
      firstName : ['', Validators.required],
      lastName  : ['', Validators.required],
      email  : ['', [Validators.maxLength(250),
        Validators.minLength(5),
Validators.pattern(/.+@.+\..+/)]],
      phone  : ['',  [Validators.pattern(/^[0-9-+()]*$/), Validators.minLength(10), Validators.maxLength(10)]],
       address   : ['', Validators.required],
       address2  : [''],
      city      : ['', Validators.required],
      state     : ['', Validators.required],
      startMonth     : [''],
      endMonth     : [''],
      postalCode: ['', [Validators.required, Validators.maxLength(6), Validators.pattern("^[0-9]*$")]],
      county_Num:[''],
  });

  this.stateFilterCtrl.valueChanges
    .pipe(takeUntil(this._onDestroy))
    .subscribe(() => {
      this.filterStates();
    });


    this.countyFilterCtrl.valueChanges
    .pipe(takeUntil(this._onDestroy))
    .subscribe(() => {
      this.filterCounty();
    });

  }

  protected filterStates() {
    if (!this.stateData) {
      return;
    }
    let search = this.stateFilterCtrl.value;
    this.filteredState = this.stateData.filter((element : any) => element.toLowerCase().indexOf(search) > -1 )
  }

  protected filterCounty() {
    if (!this.countryData) {
      return;
    }
    let search = this.countyFilterCtrl.value;
    this.filteredCounty = this.countryData.filter((element : any) => element.County.toLowerCase().indexOf(search) > -1 )
  }

  public errorHandling = (control: string, error: string) => {
    return this.form.controls[control].hasError(error);
  }
  onSubmit () {


    const formData = new FormData();
    formData.append('firstName', this.form.get('firstName').value);
    formData.append('lastName', this.form.get('lastName').value);
    formData.append('email', this.form.get('email').value);
    formData.append('phone', this.form.get('phone').value);
    formData.append('address', this.form.get('address').value);
    formData.append('address2', this.form.get('address2').value);
    formData.append('city', this.form.get('city').value);
    formData.append('state', this.form.get('state').value);
    formData.append('postalCode', this.form.get('postalCode').value);
    formData.append('startMonth', this.form.get('startMonth').value);
    formData.append('endMonth', this.form.get('endMonth').value);
    formData.append('county_Num', this.form.get('county_Num').value);
    formData.append('created_by','');
    formData.append('updated_by','');
    const id = this.form.get('id').value;

    if (id) {
      this.ProducersService.updateProducer(formData,id).subscribe(
        res => {


          if (res.status === 'fail') {
            this.formError = res.message;
            this.openSnackBar(this.formError,'Error');
          } else {
            localStorage.setItem('sucessMsg',res.message);
            this.router.navigate(['/apps/admin/manage-producers']);
          }
        },
        error => this.error = error
      );
    } else {
      this.ProducersService.createProducer(formData).subscribe(
        res => {
          if (res.status === 'fail') {
            this.formError = res.message;
            this.openSnackBar(this.formError,'Error');
          } else {
            localStorage.setItem('sucessMsg',res.message);
            this.router.navigate(['/apps/admin/manage-producers']);
          }
        },
        error => this.error = error
      );
    }
  }


  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }


}
