import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import {AssismentService} from '../../../../service/assisment.service';
import {UsersService} from '../../../../service/users.service';
import {MatSnackBar} from '@angular/material/snack-bar';
import * as moment from 'moment';
import 'moment/locale/pt-br';
import { FormControl } from '@angular/forms';
import { fuseAnimations } from '@fuse/animations';
import { takeUntil } from 'rxjs/operators';


@Component({
  selector: 'app-assessment-form',
  templateUrl: './assessment-form.component.html',
  styleUrls: ['./assessment-form.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class AssessmentFormComponent implements OnInit {

  processorData : [];

  public processor_id: FormControl = new FormControl();

  public processorFilterCtrl: FormControl = new FormControl();

  public filteredprocessor = [];
  
  protected _onDestroy = new Subject<void>(); 
  private _unsubscribeAll: Subject<any>;
/**
 * Constructor
 *
 * @param {FormBuilder} _formBuilder
 */
constructor(
  private _formBuilder: FormBuilder,
  private AssismentService : AssismentService,
  private UsersService : UsersService,
  private router: Router,
  private _snackBar: MatSnackBar,
  private route: ActivatedRoute
)
{
  // Set the private defaults
  this._unsubscribeAll = new Subject();
}


title = "Create Assesment";
form: FormGroup;
countryData = [];
accessTypeData = [
  {
    'id':1,
    'title' : 'Grain'
  },
  {
    'id':2,
    'title' : 'Silage'
  }
];
error = '';
formError = "";

date_EnteredData = "";
date_DepositedData = "";
check_DateData = "";
month = 0;
checkMonth = 0;
depositeMonth = 0;
  ngOnInit(): void {


    this.UsersService.getCountry().subscribe((data) => {
      this.countryData = data['data'];

    });

    this.UsersService.getProcessors().subscribe((data) => {
      this.processorData = data['data'];
      this.filteredprocessor = this.processorData;

    });

    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
      this.title = 'Edit Assesment';

        this.AssismentService.getAssessment(id).subscribe( (res) => {


        const schools = res['data'];
        //  let new_date_Deposited = new Date(schools.date_Deposited);
        //  let new_check_Date = new Date(schools.check_Date);
        let new_date_Deposited = schools.date_Deposited;
        let new_check_Date = schools.check_Date;
            this.form.patchValue({
              processor_id: Number(schools.processor_id),
              date_Deposited: new_date_Deposited,
              deposit_Num: schools.deposit_Num,
              check_Date: new_check_Date,
              country : Number(schools.county_id),
              check_Num: schools.check_Num,
              amount: schools.amount,
              buTons: schools.buTons,
              accessType: Number(schools.acssessType),
              id: schools.assessment_id
            });
        }

      );

    } else {
      this.title = 'Create Assesment';
    }

    // Reactive Form
     this.form = this._formBuilder.group({
      id : [''],
      processor_id : ['', Validators.required],
      date_Deposited  : ['', Validators.required],
      deposit_Num  : ['', Validators.required],
      check_Date   : ['', Validators.required],
      check_Num  : ['',Validators.required],
      amount      : ['',[ Validators.required,Validators.pattern(/^[0-9]\d*(\.\d+)?$/)]],
      buTons     : ['', [Validators.required, Validators.pattern(/^[0-9]\d*(\.\d+)?$/)]],
      accessType     : [1, Validators.required],
      country : [''],
  });

  this.processor_id.setValue(this.processorData);

  // load the initial bank list
  // this.filteredprocessor.next(this.processorData.slice());

  // listen for search field value changes
  this.processorFilterCtrl.valueChanges
    .pipe(takeUntil(this._onDestroy))
    .subscribe(() => {
      this.filterBanks();
    });
  }

  protected filterBanks() {
    if (!this.processorData) {
      return;
    }
    // get the search keyword
    let search = this.processorFilterCtrl.value;
    // if (!search) {
    //   this.filteredprocessor.next(this.processorData.slice());
    //   return;
    // } else {
    //   search = search.toLowerCase();
    // }
    // filter the banks
    this.filteredprocessor = this.processorData.filter((element : any) => element.company.toLowerCase().indexOf(search) > -1 )
    // this.filteredBanks.next(
    //   // this.processorData.filter(process => process['company'].toLowerCase().indexOf(search) > -1)
    // );
  }


  public errorHandling = (control: string, error: string) => {
    return this.form.controls[control].hasError(error);
  }

  butonsFormula(value: Number){

    // const id = this.route.snapshot.paramMap.get('id');
    // if (id) {}else{

    const accessType = this.form.get('accessType').value;
    if(accessType==1){
      
      this.form.patchValue({
        amount:  (Number(value)*0.01).toFixed(0)  
 
      });

    }else{

      this.form.patchValue({
        amount:  (Number(value)*0.074).toFixed(0) 
      });
    }
  
  //}


  }

  onSubmit () {
    if(((this.form.get('check_Date').value)._i)){
      if(((this.form.get('check_Date').value)._i).month && ((this.form.get('check_Date').value)._i).month.length){
        let checkmonthLength = ((this.form.get('check_Date').value)._i).month.length;
        this.checkMonth = (checkmonthLength!=2) ? '0'+((this.form.get('check_Date').value)._i).month : ((this.form.get('check_Date').value)._i).month;

      this.checkMonth = (this.checkMonth<13) ? Number(this.checkMonth)+Number(1) :  this.checkMonth;

      this.checkMonth = (((this.checkMonth).toString()).length!=2) ? 0+this.checkMonth : this.checkMonth;

      this.check_DateData = ((this.form.get('check_Date').value)._i).year+'-'+this.checkMonth+'-'+((this.form.get('check_Date').value)._i).date;
      }  else {
        if(((this.form.get('check_Date').value)._d)){
          let new_date = ((this.form.get('check_Date').value)._d)
          this.check_DateData = moment(new_date).format('YYYY-MM-DD')
        }
       // this.check_DateData = this.form.get('check_Date').value
      }
    }else{
      this.check_DateData = this.form.get('check_Date').value
    }

    if(((this.form.get('date_Deposited').value)._i)){
       if(((this.form.get('date_Deposited').value)._i).month && ((this.form.get('date_Deposited').value)._i).month.length){
        let monthLength = ((this.form.get('date_Deposited').value)._i).month.length;
        this.depositeMonth = (monthLength!=2) ? '0'+((this.form.get('date_Deposited').value)._i).month : ((this.form.get('date_Deposited').value)._i).month;

      this.depositeMonth = (this.depositeMonth<13) ? Number(this.depositeMonth)+Number(1) :  this.depositeMonth;

      this.depositeMonth = (((this.depositeMonth).toString()).length!=2) ? 0+this.depositeMonth : this.depositeMonth;

      this.date_DepositedData = ((this.form.get('date_Deposited').value)._i).year+'-'+this.depositeMonth+'-'+((this.form.get('date_Deposited').value)._i).date;
       } else {
        if(((this.form.get('date_Deposited').value)._d)){
          let new_date = ((this.form.get('date_Deposited').value)._d)
          this.date_DepositedData = moment(new_date).format('YYYY-MM-DD')
        }
       // this.date_DepositedData = this.form.get('date_Deposited').value
       }
  
    }else{
      this.date_DepositedData = this.form.get('date_Deposited').value
    }

    const formData = new FormData();
    formData.append('processor_id', this.form.get('processor_id').value);
    // formData.append('date_Entered',  this.date_EnteredData);
    formData.append('date_Deposited', this.date_DepositedData);
    formData.append('deposit_Num', this.form.get('deposit_Num').value);
    formData.append('check_Date', this.check_DateData);
    formData.append('check_Num', this.form.get('check_Num').value);
    formData.append('amount', this.form.get('amount').value);
    formData.append('buTons', this.form.get('buTons').value);
    formData.append('acssessType', this.form.get('accessType').value);
    formData.append('county_id', this.form.get('country').value);

    formData.append('created_by','1');
    formData.append('updated_by','1');
    const id = this.form.get('id').value;

    if (id) {
      this.AssismentService.updateAssessment(formData,id).subscribe(
        res => {


          if (res.status === 'fail') {
            this.formError = res.message;
            this.openSnackBar(this.formError,'Error');
          } else {
            localStorage.setItem('sucessMsg',res.message);
            this.router.navigate(['/apps/admin/manage-assessment']);
          }
        },
        error => this.error = error
      );
    } else {
      this.AssismentService.createAssessment(formData).subscribe(
        res => {
          if (res.status === 'fail') {
            this.formError = res.message;
            this.openSnackBar(this.formError,'Error');
          } else {
            localStorage.setItem('sucessMsg',res.message);
            this.router.navigate(['/apps/admin/manage-assessment']);
          }
        },
        error => this.error = error
      );
    }
  }


  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }




}
