import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {RefundService} from '../../../../service/refund.service'
import {UsersService} from '../../../../service/users.service';

import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';
import {ProcessorsService} from '../../../../service/processors.service';
import { ExportToCsv } from 'export-to-csv';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {ModelComponent} from '../model/model.component';
import {TextEditorComponent} from '../model/texteditor.component';
import { environment } from '../../../../../environments/environment';
@Component({
  selector: 'app-manage-processor-report',
  templateUrl: './manage-processor-report.component.html',
  styleUrls: ['./manage-processor-report.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class ManageProcessorReportComponent implements OnInit {



displayedColumns: string[] = ['id','processor', 'report','status','date','STEmail'];
dataSource = new MatTableDataSource();
@ViewChild(MatPaginator) paginator: MatPaginator;



constructor(
  private route: ActivatedRoute,
  private RefundService : RefundService,
  private UsersService : UsersService,
  private _snackBar: MatSnackBar,
  public dialog: MatDialog,
  private ProcessorsService : ProcessorsService,
  ) { }
  title = 'Report Management';
  successMsg = '';
  url = '';
  type = '';
  error = {};
  formError = "";
isLoading = false;
  mailButtonStatus = false;
  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.type = this.route.snapshot.paramMap.get('type');
    this.url = '/apps/admin/manage-'+this.type ;

    if (id) {
    this.isLoading = true;
    this.RefundService.getReportListById(id,this.type).subscribe((data) => {

      this.isLoading = false;
      const ELEMENT_DATA = data['data'];
      this.dataSource = new MatTableDataSource(ELEMENT_DATA);
      this.dataSource.paginator = this.paginator;
    },
      error => {this.error = error}
    );
  }
  }

  sendEmail(processor_id,processorReportId){
    this.mailButtonStatus = true;
    let data = {
      processor_id : processor_id,
      processorReportId:processorReportId,
      serverUrl :environment.baseUrl,
    }
    this.ProcessorsService.sendEmail(data).subscribe(
      res => {
       this.openSnackBar(res.message,'Success');
        if (res.status === 'fail') {
          this.formError = res.message;
          this.openSnackBar(this.formError,'Error');
        } else {
          localStorage.setItem('sucessMsg',res.message);
        }
        this.mailButtonStatus = false;
      },
      error => this.error = error
    );
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

  openDialog() {
  
    this.dialog.open(TextEditorComponent, {
      width: '100%',
      // data : {accessType : accessType,status:status,check_DateData : this.check_DateData,check_DateData1 : this.check_DateData1,exportData : this.exportData,'processorId':this.route.snapshot.paramMap.get('id'),'type':'assesments','mType':1,email : this.email}
    });
    // this.dialog.open(DialogOverviewExampleDialog, { panelClass: 'custom-dialog-container' })
  }
 

}

