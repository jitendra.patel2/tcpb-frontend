import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {RefundService} from '../../../../service/refund.service'
import {UsersService} from '../../../../service/users.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';

import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';


import {MatSnackBar} from '@angular/material/snack-bar'
@Component({
  selector: 'app-manage-notes',
  templateUrl: './manage-notes.component.html',
  styleUrls: ['./manage-notes.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None

})
export class ManageNotesComponent implements OnInit {
  // Private
/**
 * Constructor
 *
 * @param {FormBuilder} _formBuilder
 */
displayedColumns: string[] = ['id','notes','action'];
dataSource = new MatTableDataSource();
@ViewChild(MatPaginator) paginator: MatPaginator;

constructor(
  private _formBuilder: FormBuilder,
  private route: ActivatedRoute,
  private RefundService : RefundService,
  private UsersService : UsersService,
  private _snackBar: MatSnackBar
  ) { }

  title = 'Notes Management';
  successMsg = '';
  error = {};
  form: FormGroup;
  eId = "";
  type = "";
  url = "";
  isLoading = false;
  ngOnInit(): void {

    this.successMsg = localStorage.getItem('sucessMsg');

    this.isLoading = true; 
    if(this.successMsg){
      this.openSnackBar(this.successMsg,'Success');
      localStorage.removeItem('sucessMsg');

    }

    const cid = this.route.snapshot.paramMap.get('id');
    this.type = this.route.snapshot.paramMap.get('type');
    this.eId = this.route.snapshot.paramMap.get('id');
    this.url = '/apps/admin/manage-'+this.type ;
    if (cid) {
  
      this.RefundService.listNotes(cid).subscribe( (data) => {
          this.isLoading = false;
          const ELEMENT_DATA = data['data'];
          this.dataSource = new MatTableDataSource(ELEMENT_DATA);
          this.dataSource.paginator = this.paginator;

        }
      );
    } 


  }

  onDelete(id: String) {
    if (confirm('Are you sure want to delete this ?')) {
  
      this.RefundService.updateNotesStatus(id,2).subscribe(
        res => {
          localStorage.setItem('sucessMsg',"Notes deleted successfully.");
          this.ngOnInit();
        },
        error => this.error = error
      );
    }
  }

  onStatusChange(id: String,status: Number) {

    this.RefundService.updateNotesStatus(id,+status).subscribe(
      res => {
        localStorage.setItem('sucessMsg',res.message);
        this.ngOnInit();
      },
      error => this.error = error
    );
  
}
  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }


}
