import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import {RefundService} from '../../../../service/refund.service';
import {UsersService} from '../../../../service/users.service';
import {MatSnackBar} from '@angular/material/snack-bar';

import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';

@Component({
  selector: 'app-notes-form',
  templateUrl: './notes-form.component.html',
  styleUrls: ['./notes-form.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class NotesFormComponent implements OnInit {



// Private
private _unsubscribeAll: Subject<any>;
/**
 * Constructor
 *
 * @param {FormBuilder} _formBuilder
 */

constructor(
  private _formBuilder: FormBuilder,
  private RefundService : RefundService,
  private UsersService : UsersService,
  private router: Router,
  private _snackBar: MatSnackBar,
  private route: ActivatedRoute
)
{
  // Set the private defaults
  this._unsubscribeAll = new Subject();
}


title = "Add Note";
form: FormGroup;
nId = "";
formError = "";
error = "";
type ="";
  ngOnInit(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.nId = this.route.snapshot.paramMap.get('nId');
    this.type = this.route.snapshot.paramMap.get('type');
    if (id) {
      this.title = 'Edit Note';

  
      this.RefundService.getNote(id).subscribe( (res) => {
        const schools = res['data'];

            this.form.patchValue({
              notes: schools.notes,
              id: schools.id
            });
        }
      );
    } else {
      this.title = "Add Note";
    }

     // Reactive Form
     this.form = this._formBuilder.group({
      id : [''],
      notes : ['', Validators.required],
  
  });

  }

  public errorHandling = (control: string, error: string) => {
    return this.form.controls[control].hasError(error);
  }

  onSubmit () {


    const formData = new FormData();
    formData.append('notes', this.form.get('notes').value);
    formData.append('dataId', this.nId );
    const id = this.form.get('id').value;

    if (id) {
      this.RefundService.updateNote(formData,id).subscribe(
        res => {


          if (res.status === 'fail') {
            this.formError = res.message;
            this.openSnackBar(this.formError,'Error');
          } else {
            localStorage.setItem('sucessMsg',res.message);
            this.router.navigate(['/apps/admin/Manage-Notes/'+ this.nId +'/'+this.type]);
          }
        },
        error => this.error = error
      );
    } else {
      this.RefundService.createNote(formData).subscribe(
        res => {
          if (res.status === 'fail') {
            this.formError = res.message;
            this.openSnackBar(this.formError,'Error');
          } else {
            localStorage.setItem('sucessMsg',res.message);
            this.router.navigate(['/apps/admin/Manage-Notes/'+ this.nId+'/'+this.type ]);
          }
        },
        error => this.error = error
      );
    }
  }


  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }


}
