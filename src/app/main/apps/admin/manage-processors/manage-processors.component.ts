import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import {ProcessorsService} from '../../../../service/processors.service';

import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';

import {MatSnackBar} from '@angular/material/snack-bar';

import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import { ExportToCsv } from 'export-to-csv';

@Component({
  selector: 'app-manage-processors',
  templateUrl: './manage-processors.component.html',
  styleUrls: ['./manage-processors.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})

export class ManageProcessorsComponent implements OnInit {

  displayedColumns: string[] = ['processor_id', 'company','address1','activeMonth','action'];
  dataSource = new MatTableDataSource();
 @ViewChild(MatPaginator) paginator: MatPaginator;

  constructor(
  private route: ActivatedRoute,
  private ProcessorsService : ProcessorsService,
  private _snackBar: MatSnackBar
  ) { }
  title = 'Processors Management';
successMsg = '';
error = {};
isLoading = false;
exportData = [];
 
  ngOnInit(): void {


    this.isLoading = true;
    this.successMsg = localStorage.getItem('sucessMsg');
    
    if(this.successMsg){
      this.openSnackBar(this.successMsg,'Success');
      localStorage.removeItem('sucessMsg');

    }


    this.ProcessorsService.getProcessors('0','0','0').subscribe((data) => {
      const ELEMENT_DATA = data['data'];
      this.exportData  = data['data'];
      this.dataSource = new MatTableDataSource(ELEMENT_DATA);
      this.dataSource.paginator = this.paginator;
      this.isLoading = false;
    },
      error => {this.error = error}
    );
  }

public doFilter = (value: string) => {
  this.isLoading = true;
  const search = (value.trim().toLocaleLowerCase()) ? value.trim().toLocaleLowerCase() : '0';
  this.ProcessorsService.getProcessors('0','0',search).subscribe((data) => {
    const ELEMENT_DATA = data['data'];
    this.exportData  = data['data'];
    this.dataSource = new MatTableDataSource(ELEMENT_DATA);
    this.dataSource.paginator = this.paginator;
    this.isLoading = false;
    },
      error => {this.error = error}
    );

}


  onDelete(id: String) {
    if (confirm('Are you sure want to delete this ?')) {
  
      this.ProcessorsService.updateStatusProcessor(id,2).subscribe(
        res => {
          localStorage.setItem('sucessMsg',"Processor deleted successfully.");
          this.ngOnInit();
        },
        error => this.error = error
      );
    }
  }

  onStatusChange(id: String,status: Number) {

    this.ProcessorsService.updateStatusProcessor(id,+status).subscribe(
      res => {
        localStorage.setItem('sucessMsg',res.message);
        this.ngOnInit();
      },
      error => this.error = error
    );
  
}


exportCsv() {
  // this.StudentService.getStudent(id).subscribe( (res) => {
  //     const csvExporter = new ExportToCsv(res['data']);
  //     csvExporter.generateCsv(res['data']);
  //   }
  // );

  var data = this.exportData;
  for(let i in data){
    if(data[i].monthly_Report == 1){
      data[i].monthly_Report = 'Yes';
    } else {
      data[i].monthly_Report = 'No';
    }

    if(data[i].assessType == "1"){
      data[i].assessType = 'Grain';
    } else {
      data[i].assessType = 'Silage';
    }

    if(data[i].status == 0){
      data[i].status = 'Archived';
    } else if(data[i].status == 1) {
      data[i].status = 'Active';
    } else {
      data[i].status = 'Deleted';
    }
  }
  this.exportData = this.exportData.filter(item => item.status !== status); 
  
    const options = { 
      fieldSeparator: ',',
      quoteStrings: '"',
      decimalSeparator: '.',
      showLabels: true, 
      showTitle: true,
      title: 'Processors List',
      useTextFile: false,
      useBom: true,
      useKeysAsHeaders: true,
      // headers: ['Column 1', 'Column 2', etc...] <-- Won't work with useKeysAsHeaders present!
    };
   
  const csvExporter = new ExportToCsv(options);
   
  csvExporter.generateCsv(data);


}
  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

}
