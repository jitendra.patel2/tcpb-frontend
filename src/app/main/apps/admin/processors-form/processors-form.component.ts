import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'
import {MatSnackBar} from '@angular/material/snack-bar';
import { Subject } from 'rxjs';
import {ProcessorsService} from '../../../../service/processors.service';
import {UsersService} from '../../../../service/users.service';
import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';


@Component({
  selector: 'app-processors-form',
  templateUrl: './processors-form.component.html',
  styleUrls: ['./processors-form.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class ProcessorsFormComponent implements OnInit {

   // Private
   private _unsubscribeAll: Subject<any>;
   /**
    * Constructor
    *
    * @param {FormBuilder} _formBuilder
    */
   constructor(
     private _formBuilder: FormBuilder,
     private ProcessorsService : ProcessorsService,
     private UsersService : UsersService,
     private router: Router,
     private route: ActivatedRoute,
     private _snackBar: MatSnackBar
 )
 {
     // Set the private defaults
     this._unsubscribeAll = new Subject();
 }


 title = "Create Processors";
 form: FormGroup;
 countryData : [];
 stateData : [];

 roleData = [
    {
      'id':1,
      'role' : 'Yes'
    },
    {
      'id':2,
      'role' : 'No'
    }
  ];

  accessTypeData = [
    {
      'id':1,
      'title' : 'Grain'
    },
    {
      'id':2,
      'title' : 'Silage'
    }
  ];

  pTypeData = [
    {
      'id':1,
      'title' : 'Elevator'
    },
    {
      'id':2,
      'title' : 'FSA Office'
    }
  ];

  monthData = ['JAN', 'FEB', 'MAR', 'APR', 'MAY', 'JUN', 'JUL', 'AUG', 'SEP', 'OCT', 'NOV', 'DEC'];
 yearData = ['2019','2020','2021','2022'];
 error = '';
 formError = "";
  ngOnInit(): void {
   
    this.UsersService.getState().subscribe((data) => {
      this.stateData = data['data'];

    });
    
    this.UsersService.getCountry().subscribe((data) => {
      this.countryData = data['data'];

    });

    const id = this.route.snapshot.paramMap.get('id');
    if (id) {
      this.title = 'Edit Processor';

  
      this.ProcessorsService.getProcessor(id).subscribe( (res) => {
        const schools = res['data'];

        this.title = 'Edit '+schools.company;

          this.form.patchValue({
            company: schools.company,
            email: schools.email,
            phone: schools.phone,
            address: schools.address1,
            address2: schools.address2,
            city: schools.city,
            state: schools.state,
            postalCode: schools.zip,
            county_Num:schools.county_Num,
            monthly_Report: schools.monthly_Report,
            accessType: Number(schools.assessType),
            pType: Number(schools.type),
            STEmail: schools.STEmail,
            elevGroup: schools.elevGroup,
            startMonth: schools.startMonth,
            endMonth: schools.endMonth,
            startyear: schools.startyear,
            endyear: schools.endyear,
            // send_email: schools.send_email,
            id: schools.processor_id
          });

        }
      );
    } else {
      this.title = 'Create Processor';
    }



     // Reactive Form
     this.form = this._formBuilder.group({
      id : [''],
      company : ['', Validators.required],
      email  : ['', [Validators.maxLength(250),
        Validators.minLength(5),
Validators.pattern(/.+@.+\..+/)]],
      phone  : ['',  [Validators.pattern(/^[0-9-+()]*$/), Validators.minLength(10), Validators.maxLength(10)]],
      address   : ['', Validators.required],
      address2  : [''],
      STEmail  : [''],
      city      : ['', Validators.required],
      state     : ['', Validators.required],
      postalCode: ['', [Validators.required, Validators.maxLength(6), Validators.pattern("^[0-9]*$")]],
      county_Num:[''],
      monthly_Report     : ['', Validators.required],
      accessType     : ['', Validators.required],
      pType     : ['', Validators.required],
      elevGroup     : [''],
      startMonth     : [''],
      endMonth     : [''],
      startyear     : [''],
      endyear     : [''],
      // send_email  : [''],
  });

  }

  public errorHandling = (control: string, error: string) => {
    return this.form.controls[control].hasError(error);
  }
  onSubmit () {


    const formData = new FormData();
    // if(this.form.get('STEmail').value == true && this.form.get('email').value){
    //   formData.append('STEmail', this.form.get('STEmail').value);
    // } else {
    //   formData.append('STEmail', this.form.get('STEmail').value);
    // }
    formData.append('company', this.form.get('company').value);
    formData.append('email', this.form.get('email').value);
    formData.append('phone', this.form.get('phone').value);
    formData.append('address', this.form.get('address').value);
    formData.append('address2', this.form.get('address2').value);
    formData.append('city', this.form.get('city').value);
    formData.append('state', this.form.get('state').value);
    formData.append('postalCode', this.form.get('postalCode').value);
    formData.append('monthly_Report', this.form.get('monthly_Report').value);
    formData.append('assessType', this.form.get('accessType').value);
    formData.append('pType', this.form.get('pType').value);
    formData.append('elevGroup', this.form.get('elevGroup').value);
    formData.append('startMonth', this.form.get('startMonth').value);
    formData.append('endMonth', this.form.get('endMonth').value);
    formData.append('startyear', this.form.get('startyear').value);
    formData.append('endyear', this.form.get('endyear').value);
     formData.append('STEmail', this.form.get('STEmail').value);
    formData.append('county_Num', this.form.get('county_Num').value);
    formData.append('created_by','');
    formData.append('updated_by','');

    const id = this.form.get('id').value;
    

    if (id) {
      this.ProcessorsService.updateProcessor(formData,id).subscribe(
        res => {


          if (res.status === 'fail') {
            this.formError = res.message;
            this.openSnackBar(this.formError,'Error');
          } else {
            localStorage.setItem('sucessMsg',res.message);
            this.router.navigate(['/apps/admin/manage-processors']);
          }
        },
        error => this.error = error
      );
    } else {
      this.ProcessorsService.createProcessor(formData).subscribe(
        res => {
          if (res.status === 'fail') {
            this.formError = res.message;
            this.openSnackBar(this.formError,'Error');
          } else {
            localStorage.setItem('sucessMsg',res.message);
            this.router.navigate(['/apps/admin/manage-processors']);
          }
        },
        error => this.error = error
      );
    }
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }



}
