import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import {UsersService} from '../../../../service/users.service';
import {MatSnackBar} from '@angular/material/snack-bar';;

import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';

@Component({
  selector: 'app-changePassword-form',
  templateUrl: './changePassword-form.component.html',
  styleUrls: ['./changePassword-form.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class ChangePasswordFormComponent implements OnInit {


// Private
private _unsubscribeAll: Subject<any>;
/**
 * Constructor
 *
 * @param {FormBuilder} _formBuilder
 */
constructor(
  private _formBuilder: FormBuilder,
      private UsersService : UsersService,
      private router: Router,
      private route: ActivatedRoute,
      private _snackBar: MatSnackBar
)
{
  // Set the private defaults
  this._unsubscribeAll = new Subject();
}


title = "Change Passwod";
form: FormGroup;
error = '';
formError = "";
successMsg = '';
process_DateData = "";
check_DateData = "";
month = 0;
checkMonth = 0;
pp = true
  ngOnInit(): void {
    this.title = 'Change Password';
   // Reactive Form
   this.form = this._formBuilder.group({
    email  : ['', [Validators.required, Validators.maxLength(250),Validators.minLength(5),Validators.pattern(/.+@.+\..+/)]],
    oldPassword  : ['', [Validators.required, Validators.minLength(5), Validators.maxLength(15)]],
    newPassword  : ['', [Validators.required, Validators.minLength(5), Validators.maxLength(15), Validators.pattern(/^(?=\D*\d)(?=[^a-z]*[a-z])(?=[^A-Z]*[A-Z]).{8,30}$/)]],
    confirmPassword: ['']
  }, {
   validators: this.checkPasswords 
  });


  }

  checkPasswords(group: FormGroup) { // here we have the 'passwords' group
  const password = group.get('newPassword').value;
  const confirmPassword = group.get('confirmPassword').value;
  // if(password === confirmPassword){
  //   return true;
  // } else {return ''}
  return password === confirmPassword ? null : { notSame: true } 
     
}



  public errorHandling = (control: string, error: string) => {
    return this.form.controls[control].hasError(error);
  }

  onSubmit () {

    const currentUser = JSON.parse(localStorage.getItem('currentUser'));
    const formData = new FormData();
    // formData.append('id', currentUser.id);
    formData.append('email', this.form.get('email').value);
    formData.append('newPassword', this.form.get('newPassword').value);
    formData.append('oldPassword', this.form.get('oldPassword').value);

      this.UsersService.changeAdminPassword(formData,currentUser.id).subscribe(
        res => {
          if (res.status === 'fail') {
            this.formError = res.message;
            this.openSnackBar(this.formError,'Error');
          } else {
            this.successMsg = res.message;
            this.openSnackBar(this.successMsg,'Success');
          }
        },
        error => this.error = error
      );
    
  }

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }
}