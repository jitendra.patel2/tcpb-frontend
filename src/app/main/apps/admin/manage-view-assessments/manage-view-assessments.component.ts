import { Component, ElementRef, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import {AssismentService} from '../../../../service/assisment.service'
import { fuseAnimations } from '@fuse/animations';
import { FuseUtils } from '@fuse/utils';
import {UsersService} from '../../../../service/users.service';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {ProcessorsService} from '../../../../service/processors.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MatSnackBar} from '@angular/material/snack-bar';
import { ExportToCsv } from 'export-to-csv';
import {ModelComponent} from '../model/model.component';
@Component({
  selector: 'app-manage-view-assessments',
  templateUrl: './manage-view-assessments.component.html',
  styleUrls: ['./manage-view-assessments.component.scss'],
  animations   : fuseAnimations,
  encapsulation: ViewEncapsulation.None
})
export class ManageViewAssessmentsComponent implements OnInit {


/**
 * Constructor
 *
 * @param {FormBuilder} _formBuilder
 */
displayedColumns: string[] = ['assessment_id','processor_id', 'acssessType','buTons', 'amount','action'];
dataSource = new MatTableDataSource();
@ViewChild(MatPaginator) paginator: MatPaginator;

constructor(
  private _formBuilder: FormBuilder,
  private route: ActivatedRoute,
  private AssismentService : AssismentService,
  private UsersService : UsersService,
  private ProcessorsService : ProcessorsService,
  public dialog: MatDialog,
  private _snackBar: MatSnackBar
  ) { 


  }
  title = 'Assessments Management';
  form: FormGroup;
  email = '';
successMsg = '';
error = {};
//dataSource = [];
processorData = [];
checkMonth = 0;
checkMonth1 = 0;
check_DateData = '0';
check_DateData1 = '0';
  isLoading = false;
  exportData = [];
  url = "";
  type = "";
accessTypeData = [
  {
    'id':1,
    'title' : 'Grain'
  },
  {
    'id':2,
    'title' : 'Corn'
  }
];
  ngOnInit(): void {

    this.isLoading = true;

    const id = this.route.snapshot.paramMap.get('id');
    this.type = this.route.snapshot.paramMap.get('type');
    this.url = '/apps/admin/manage-'+this.type ;

    if (id) {

        this.ProcessorsService.getProcessor(id).subscribe( (res) => {


          const schools = res['data'];
          this.title = schools.company;
          this.email = schools.email;
        });
      
        this.AssismentService.getAssessments(id,'0','0','0','0','0','0').subscribe((data) => {
          const ELEMENT_DATA = data['data'];
          this.exportData = data['data'];
          this.isLoading = false;
      this.dataSource = new MatTableDataSource(ELEMENT_DATA);

      this.dataSource.paginator = this.paginator;


        },
          error => {this.error = error}
        );
    }




    this.UsersService.getProcessors().subscribe((data) => {
      this.processorData = data['data'];

    });


    this.successMsg = localStorage.getItem('sucessMsg');

    
    if(this.successMsg){
      this.openSnackBar(this.successMsg,'Success');
      localStorage.removeItem('sucessMsg');

    }
        // Reactive Form
        this.form = this._formBuilder.group({
          processor_id : [''],
          accessType  : [''],
          check_Date  : [''],
          check_Date1  : [''],
          status : ['1']
      });

 
    localStorage.removeItem('processor_id');
  }

  filterChange(value: String,event: String){
    this.isLoading = true;
    const search = (value.trim().toLocaleLowerCase()) ? value.trim().toLocaleLowerCase() : '0';

    
    
    if(((this.form.get('check_Date1').value)._i)){
      
      this.checkMonth1 = (((this.form.get('check_Date1').value)._i).month.length!=2) ? '0'+((this.form.get('check_Date1').value)._i).month : ((this.form.get('check_Date1').value)._i).month;

      this.checkMonth1 = (this.checkMonth1<13) ? Number(this.checkMonth1)+Number(1) :  this.checkMonth1;

      this.checkMonth1 = (((this.checkMonth1).toString()).length!=2) ? 0+this.checkMonth1 : this.checkMonth1;

      this.check_DateData1 = ((this.form.get('check_Date1').value)._i).year+'-'+this.checkMonth1+'-'+((this.form.get('check_Date1').value)._i).date;

    }else{
      this.check_DateData1 = this.form.get('check_Date1').value
    }

    this.check_DateData1 = (this.check_DateData1) ? this.check_DateData1 : '0';

    
    if(((this.form.get('check_Date').value)._i)){
      
      this.checkMonth = (((this.form.get('check_Date').value)._i).month.length!=2) ? '0'+((this.form.get('check_Date').value)._i).month : ((this.form.get('check_Date').value)._i).month;

      this.checkMonth = (this.checkMonth<13) ? Number(this.checkMonth)+Number(1) :  this.checkMonth;

      this.checkMonth = (((this.checkMonth).toString()).length!=2) ? 0+this.checkMonth : this.checkMonth;

      this.check_DateData = ((this.form.get('check_Date').value)._i).year+'-'+this.checkMonth+'-'+((this.form.get('check_Date').value)._i).date;

    }else{
      this.check_DateData = this.form.get('check_Date').value
    }

    this.check_DateData = (this.check_DateData) ? this.check_DateData : '0';

  // const processor_id = (this.form.get('processor_id').value) ? this.form.get('processor_id').value : '0';
   const accessType = (this.form.get('accessType').value) ? this.form.get('accessType').value : '0';
   const status = (this.form.get('status').value) ? this.form.get('status').value : '0';


   const processorD_id = this.route.snapshot.paramMap.get('id');

   this.AssismentService.getAssessments(processorD_id,accessType,this.check_DateData,search,status,this.check_DateData1,'0').subscribe((data) => {
    const ELEMENT_DATA = data['data'];
    this.exportData = data['data'];

    this.isLoading = false;
 this.dataSource = new MatTableDataSource(ELEMENT_DATA);

 this.dataSource.paginator = this.paginator;

    },
      error => {this.error = error}
    );
  
  }


  

  sendReport(){

    const accessType = (this.form.get('accessType').value) ? this.form.get('accessType').value : '0';
    const status = (this.form.get('status').value) ? this.form.get('status').value : '0';

    const dialogRef = this.dialog.open(ModelComponent, {
      width: '100%',
      data : {accessType : accessType,status:status,check_DateData : this.check_DateData,check_DateData1 : this.check_DateData1,exportData : this.exportData,'processorId':this.route.snapshot.paramMap.get('id'),'type':'assesments','mType':1,email : this.email}
    });

  }

  exportCsv() {
    // this.StudentService.getStudent(id).subscribe( (res) => {
    //     const csvExporter = new ExportToCsv(res['data']);
    //     csvExporter.generateCsv(res['data']);
    //   }
    // );

    var data = this.exportData;
    this.exportData = this.exportData.filter(item => item.status !== status); 
    
      const options = { 
        fieldSeparator: ',',
        quoteStrings: '"',
        decimalSeparator: '.',
        showLabels: true, 
        showTitle: true,
        title: 'Assessments List',
        useTextFile: false,
        useBom: true,
        useKeysAsHeaders: true,
        // headers: ['Column 1', 'Column 2', etc...] <-- Won't work with useKeysAsHeaders present!
      };
     
    const csvExporter = new ExportToCsv(options);
     
    csvExporter.generateCsv(data);
 
  
  }

  onDelete(id: String) {
    
    if (confirm('Are you sure want to delete this ?')) {
  
      this.AssismentService.updateStatusAssessment(id,2).subscribe(
        res => {
          localStorage.setItem('sucessMsg',"Processor deleted successfully.");
          this.ngOnInit();
        },
        error => this.error = error
      );
    }
  }

  onStatusChange(id: String,status: Number) {

    this.AssismentService.updateStatusAssessment(id,+status).subscribe(
      res => {
        localStorage.setItem('sucessMsg',res.message);
        this.ngOnInit();
      },
      error => this.error = error
    );
  
}

  openSnackBar(message: string, action: string) {
    this._snackBar.open(message, action, {
      duration: 2000,
    });
  }

}
