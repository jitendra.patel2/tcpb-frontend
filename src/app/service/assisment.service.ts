import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse,HttpHeaders} from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class AssismentService {

  serverUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }

  getAssessments(filter:String,id:String,date:String,search:String,status:String,date1:String,deposit_Num:String) {
    return this.http.get<any>(this.serverUrl + 'api/admin/listAssessments/'+filter+'/'+id+'/'+date+'/'+search+'/'+status+'/'+date1+'/'+deposit_Num).pipe(
      catchError(this.handleError)
    );
  }

  getAssessments1(filter:String,id:String,date:String,search:String,status:String,date1:String,deposit_Num:String) {
    return this.http.get<any>(this.serverUrl + 'api/admin/listAssessments1/'+filter+'/'+id+'/'+date+'/'+search+'/'+status+'/'+date1+'/'+deposit_Num).pipe(
      catchError(this.handleError)
    );
  }

  
  getAssessment(id: String) {
    return this.http.get<any>(this.serverUrl + 'api/admin/getAssessmentsById/'+id).pipe(
      catchError(this.handleError)
    );
  }

  deleteAssessment(id: String) {
     
    return this.http.get<any>(this.serverUrl + 'api/admin/updateAssessmentsStatus/' + id).pipe(
      catchError(this.handleError)
    );
  }

  createAssessment(school) {
    return this.http.post<any>(this.serverUrl + 'api/admin/createAssessments', school)
    .pipe(
      catchError(this.handleError)
    );
  }

  updateAssessment(school, id: String) {
    return this.http.post<any>(this.serverUrl + 'api/admin/updateAssessments/' + id, school)
    .pipe(
      catchError(this.handleError)
    );
  }

  updateStatusAssessment(id: String,status: Number) {
    
    return this.http.get<any>(this.serverUrl + 'api/admin/updateAssessmentsStatus/' + id+'/'+status).pipe(
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {

      // A client-side or network error occurred. Handle it accordingly.

      console.error('An error occurred:', error.error.message);
    } else {

      // The backend returned an unsuccessful response code.

      // The response body may contain clues as to what went wrong.

      console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
    }

    // return an observable with a user-facing error message
    
    return throwError('Something bad happened. Please try again later.');
  }



}
