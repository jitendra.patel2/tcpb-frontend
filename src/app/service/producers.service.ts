import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse,HttpHeaders} from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ProducersService {

  serverUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }

  getProducers(filter:String,schoolId:String,search:string) {
    return this.http.get<any>(this.serverUrl + 'api/admin/listProducers/'+search).pipe(
      catchError(this.handleError)
    );
  }

  getProducer(id: String) {
    return this.http.get<any>(this.serverUrl + 'api/admin/getProducersById/'+id).pipe(
      catchError(this.handleError)
    );
  }
  deleteProducer(id: String) {
     
    return this.http.get<any>(this.serverUrl + 'api/admin/updateProducersStatus/' + id).pipe(
      catchError(this.handleError)
    );
  }

  createProducer(school) {
    return this.http.post<any>(this.serverUrl + 'api/admin/createProducers', school)
    .pipe(
      catchError(this.handleError)
    );
  }

  updateProducer(school, id: String) {
    return this.http.post<any>(this.serverUrl + 'api/admin/updateProducers/' + id, school)
    .pipe(
      catchError(this.handleError)
    );
  }

  updateStatusProducer(id: String,status: Number) {
    
    return this.http.get<any>(this.serverUrl + 'api/admin/updateProducersStatus/' + id+'/'+status).pipe(
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {

      // A client-side or network error occurred. Handle it accordingly.

      console.error('An error occurred:', error.error.message);
    } else {

      // The backend returned an unsuccessful response code.

      // The response body may contain clues as to what went wrong.

      console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
    }

    // return an observable with a user-facing error message
    
    return throwError('Something bad happened. Please try again later.');
  }
}
