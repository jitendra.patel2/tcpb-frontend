import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse,HttpHeaders} from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class UsersService {

  serverUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }
  getUsers(filter:String,schoolId:String,search:string) {
    return this.http.get<any>(this.serverUrl + 'api/admin/listUser/'+search).pipe(
      catchError(this.handleError)
    );
  }

  getState() {
    return this.http.get<any>(this.serverUrl + 'api/admin/getState/').pipe(
      catchError(this.handleError)
    );
  }

  

  getCountry() {
    return this.http.get<any>(this.serverUrl + 'api/admin/countiesList').pipe(
      catchError(this.handleError)
    );
  }

  getProcessors() {
    return this.http.get<any>(this.serverUrl + 'api/admin/getProcessors').pipe(
      catchError(this.handleError)
    );
  }

  getProducers() {
    return this.http.get<any>(this.serverUrl + 'api/admin/getProducers').pipe(
      catchError(this.handleError)
    );
  }



  getRole() {
    return this.http.get<any>(this.serverUrl + 'api/admin/roleList').pipe(
      catchError(this.handleError)
    );
  }

  getUser(id: String) {
    return this.http.get<any>(this.serverUrl + 'api/admin/getUserById/'+id).pipe(
      catchError(this.handleError)
    );
  }
  deleteUser(id: String) {
     
    return this.http.get<any>(this.serverUrl + 'api/admin/updateUserStatus/' + id).pipe(
      catchError(this.handleError)
    );
  }

  createUser(school) {
    return this.http.post<any>(this.serverUrl + 'api/admin/createUser', school)
    .pipe(
      catchError(this.handleError)
    );
  }

  updateUser(school, id: String) {
    return this.http.post<any>(this.serverUrl + 'api/admin/updateUser/' + id, school)
    .pipe(
      catchError(this.handleError)
    );
  }

  updateStatusUser(id: String,status: Number) {
    
    return this.http.get<any>(this.serverUrl + 'api/admin/updateUserStatus/' + id+'/'+status).pipe(
      catchError(this.handleError)
    );
  }

  changeAdminPassword(data,id:String) {
    return this.http.post<any>(this.serverUrl + 'api/admin/changePassword/' + id, data)
    .pipe(
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {

      // A client-side or network error occurred. Handle it accordingly.

      console.error('An error occurred:', error.error.message);
    } else {

      // The backend returned an unsuccessful response code.

      // The response body may contain clues as to what went wrong.

      console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
    }

    // return an observable with a user-facing error message
    
    return throwError('Something bad happened. Please try again later.');
  }


}
