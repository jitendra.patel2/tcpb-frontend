import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse,HttpHeaders} from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class ProcessorsService {

  serverUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }

  getDashboardCount() {
    return this.http.get<any>(this.serverUrl + 'api/admin/getDashboardCount').pipe(
      catchError(this.handleError)
    );
  }

  saveQuery(school) {
    return this.http.post<any>(this.serverUrl + 'api/admin/saveQuery', school)
    .pipe(
      catchError(this.handleError)
    );
  }

  getProcessors(filter:String,schoolId:String,search:string) {
    return this.http.get<any>(this.serverUrl + 'api/admin/listUProcessors/'+search).pipe(
      catchError(this.handleError)
    );
  }

  getProcessor(id: String) {
    return this.http.get<any>(this.serverUrl + 'api/admin/getProcessorsById/'+id).pipe(
      catchError(this.handleError)
    );
  }
  deleteProcessor(id: String) {
     
    return this.http.get<any>(this.serverUrl + 'api/admin/updateProcessorsStatus/' + id).pipe(
      catchError(this.handleError)
    );
  }

  createProcessor(school) {
    return this.http.post<any>(this.serverUrl + 'api/admin/createProcessors', school)
    .pipe(
      catchError(this.handleError)
    );
  }


  sendAssmentReport(school) {
    return this.http.post<any>(this.serverUrl + 'api/admin/sendAssmentReport', school)
    .pipe(
      catchError(this.handleError)
    );
  }

  sendEmail(school) {
    // let new_date = new Date();
    // console.log('111111111111111111',new_date);
    // let new_date1 = new Date().toString();
    // console.log('22222222222222',new_date1);
     school.new_date = new Date().toString();
    return this.http.post<any>(this.serverUrl + 'api/admin/sendEmail', school)
    .pipe(
      catchError(this.handleError)
    );
  }


  updateProcessor(school, id: String) {
    return this.http.post<any>(this.serverUrl + 'api/admin/updateProcessors/' + id, school)
    .pipe(
      catchError(this.handleError)
    );
  }

  updateStatusProcessor(id: String,status: Number) {
    
    return this.http.get<any>(this.serverUrl + 'api/admin/updateProcessorsStatus/' + id+'/'+status).pipe(
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {

      // A client-side or network error occurred. Handle it accordingly.

      console.error('An error occurred:', error.error.message);
    } else {

      // The backend returned an unsuccessful response code.

      // The response body may contain clues as to what went wrong.

      console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
    }

    // return an observable with a user-facing error message
    
    return throwError('Something bad happened. Please try again later.');
  }
}
