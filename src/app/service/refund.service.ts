import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse,HttpHeaders} from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';
@Injectable({
  providedIn: 'root'
})
export class RefundService {

  serverUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }

  listQuery() {
    return this.http.get<any>(this.serverUrl + 'api/admin/listQuery').pipe(
      catchError(this.handleError)
    );
  } 


  getQueryById(id: String) {
    return this.http.get<any>(this.serverUrl + 'api/admin/getQueryById/'+id).pipe(
      catchError(this.handleError)
    );
  }

  getReportListById(id: String,type:String) {
    return this.http.get<any>(this.serverUrl + 'api/admin/getReportListById/'+id+'/'+type).pipe(
      catchError(this.handleError)
    );
  }

  getReportList() {
    return this.http.get<any>(this.serverUrl + 'api/admin/getReportList').pipe(
      catchError(this.handleError)
    );
  } 

  getProcessorCount(date:String,status:String,date1:String,stEmail:String,processor_type:String) {
    return this.http.get<any>(this.serverUrl + 'api/admin/getProcessorCount/'+date+'/'+status+'/'+date1+'/'+stEmail+'/'+processor_type).pipe(
      catchError(this.handleError)
    );
  } 


  getReport(date:String,status:String,date1:String,sReportType:String,stEmail:String,processor_type:String) {
    return this.http.get<any>(this.serverUrl + 'api/admin/getReport/'+date+'/'+status+'/'+date1+'/'+sReportType+'/'+stEmail+'/'+processor_type).pipe(
      catchError(this.handleError)
    );
  } 

  getRefunds(filter:String,id:String,date:String,search:String,status:String,date1:String) {
    return this.http.get<any>(this.serverUrl + 'api/admin/listRefund/'+filter+'/'+id+'/'+date+'/'+search+'/'+status+'/'+date1).pipe(
      catchError(this.handleError)
    );
  }

  getRefunds1(filter:String,id:String,date:String,search:String,status:String,date1:String) {
    return this.http.get<any>(this.serverUrl + 'api/admin/listRefund1/'+filter+'/'+id+'/'+date+'/'+search+'/'+status+'/'+date1).pipe(
      catchError(this.handleError)
    );
  }

  createNote(school) {
    return this.http.post<any>(this.serverUrl + 'api/admin/createNotes', school)
    .pipe(
      catchError(this.handleError)
    );
  }

  updateNote(school, id: String) {
    return this.http.post<any>(this.serverUrl + 'api/admin/updateNotes/' + id, school)
    .pipe(
      catchError(this.handleError)
    );
  }

  getNote(id: String) {
    return this.http.get<any>(this.serverUrl + 'api/admin/geNotesById/'+id).pipe(
      catchError(this.handleError)
    );
  }

  listNotes(id: String) {
    return this.http.get<any>(this.serverUrl + 'api/admin/listNotes/'+id).pipe(
      catchError(this.handleError)
    );
  }


  updateNotesStatus(id: String,status: Number) {
    
    return this.http.get<any>(this.serverUrl + 'api/admin/updateNotesStatus/' + id+'/'+status).pipe(
      catchError(this.handleError)
    );
  }
  
  getRefund(id: String) {
    return this.http.get<any>(this.serverUrl + 'api/admin/getRefundById/'+id).pipe(
      catchError(this.handleError)
    );
  }

  deleteRefund(id: String) {
     
    return this.http.get<any>(this.serverUrl + 'api/admin/updateRefundStatus/' + id).pipe(
      catchError(this.handleError)
    );
  }

  createRefund(school) {
    return this.http.post<any>(this.serverUrl + 'api/admin/createRefund', school)
    .pipe(
      catchError(this.handleError)
    );
  }

  updateRefund(school, id: String) {
    return this.http.post<any>(this.serverUrl + 'api/admin/updateRefund/' + id, school)
    .pipe(
      catchError(this.handleError)
    );
  }

  updateStatusRefund(id: String,status: Number) {
    
    return this.http.get<any>(this.serverUrl + 'api/admin/updateRefundStatus/' + id+'/'+status).pipe(
      catchError(this.handleError)
    );
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {

      // A client-side or network error occurred. Handle it accordingly.

      console.error('An error occurred:', error.error.message);
    } else {

      // The backend returned an unsuccessful response code.

      // The response body may contain clues as to what went wrong.

      console.error(`Backend returned code ${error.status}, ` + `body was: ${error.error}`);
    }

    // return an observable with a user-facing error message
    
    return throwError('Something bad happened. Please try again later.');
  }


}
