import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
        {
            id       : 'dashboards',
            title    : 'Dashboards',
            translate: 'NAV.DASHBOARDS',
            type     : 'item',
            icon     : 'dashboard',
            url  : '/apps/dashboards/analytics',
        },

        {
            id       : 'manage-producers',
            title    : 'Manage Producers',
            type     : 'collapsable',
            translate:  'Manage Producers',
            icon     : 'assignment_ind',
            children : [
                {
                    id        : 'list-producers',
                    title     : 'List Producers',
                    type      : 'item',
                    url       : '/apps/admin/manage-producers',
                },
                {
                    id        : 'create-producer',
                    title     : 'Create Producer',
                    url       : '/apps/admin/Create-Producers',
                    type      : 'item',
                }
            ]
        },

        {
            id       : 'manage-refunds',
            title    : 'Manage Refunds',
            type     : 'collapsable',
            translate:  'Manage Refunds',
            icon     : 'assignment',
            children : [
                {
                    id        : 'list-refunds',
                    title     : 'List Refunds',
                    type      : 'item',
                    url       : '/apps/admin/manage-refund',
                },
                {
                    id        : 'create-refund',
                    title     : 'Create Refund',
                    url       : '/apps/admin/Create-Refund',
                    type      : 'item',
                }
            ]
        },  

        {
            id       : 'manage-assessments',
            title    : 'Manage Assessments',
            type     : 'collapsable',
            translate:  'Manage Assessments',
            icon     : 'assignment',
            children : [
                {
                    id        : 'list-assessments',
                    title     : 'List Assessments',
                    type      : 'item',
                    url       : '/apps/admin/manage-assessment',
                },
                {
                    id        : 'create-assessment',
                    title     : 'Create Assessment',
                    url       : '/apps/admin/Create-Assessment',
                    type      : 'item',
                }
            ]
        },
       
        {
            id       : 'manage-processors',
            title    : 'Manage Processors',
            type     : 'collapsable',
            translate:  'Manage Processors',
            icon     : 'assignment',
            children : [
                {
                    id        : 'list-processors',
                    title     : 'List Processors',
                    type      : 'item',
                    url       : '/apps/admin/manage-processors',
                },
                {
                    id        : 'create-processor',
                    title     : 'Create Processor',
                    url       : '/apps/admin/Create-Processors',
                    type      : 'item',
                }
            ]
        },

        {
            id       : 'manage-users',
            title    : 'Manage Users',
            type     : 'collapsable',
            translate:  'Manage Users',
            icon     : 'person',
            children : [
                {
                    id        : 'list-users',
                    title     : 'List Users',
                    type      : 'item',
                    url       : '/apps/admin/manage-users',
                },
                {
                    id        : 'create-user',
                    title     : 'Create User',
                    url       : '/apps/admin/Create-User',
                    type      : 'item',
                }
            ]
        },


        {
            id       : 'manage-reports',
            title    : 'Manage Reports',
            type     : 'collapsable',
            translate:  'Manage Reports',
            icon     : 'report',
            children : [
                {
                    id        : 'list-reports',
                    title     : 'Send Reports',
                    type      : 'item',
                    url       : '/apps/admin/manage-reports-procesor',
                },
                {
                    id        : 'list-reports-processor',
                    title     : 'Reports Sent',
                    type      : 'item',
                    url       : '/apps/admin/manage-reports',
                },
                // {
                //     id        : 'list-query',
                //     title     : 'List Saved Query',
                //     type      : 'item',
                //     url       : '/apps/admin/manage-query',
                // }
            ]
        },
    
];
